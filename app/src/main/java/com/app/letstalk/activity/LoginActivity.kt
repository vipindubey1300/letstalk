package com.app.letstalk.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.letstalk.R
import com.app.letstalk.fragments.LoginFragment
import com.app.letstalk.fragments.SignupFragment

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.login_frame, SignupFragment()).commit()
        }

    }
}
