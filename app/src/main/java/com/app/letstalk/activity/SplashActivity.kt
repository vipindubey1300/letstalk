package com.app.letstalk.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


import android.R
import android.os.Handler


class SplashActivity : AppCompatActivity() {

    /** Duration of wait  */
    private val SPLASH_DISPLAY_LENGTH : Long = 1000
    var loginStatus = false
    private lateinit var mainIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.app.letstalk.R.layout.activity_splash)

        val PREFS_FILENAME = "user_details"
        val prefs = applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
           loginStatus = true
        }



        Handler().postDelayed(object:Runnable {
            public override fun run() {
                /* Create an Intent that will start the Menu-Activity. */


                if(loginStatus)  mainIntent = Intent(this@SplashActivity, HomeActivity::class.java)
                else  mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)


                this@SplashActivity.startActivity(mainIntent)
                this@SplashActivity.finish()
            }
        }, SPLASH_DISPLAY_LENGTH)
    }
}
