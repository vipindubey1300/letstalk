package com.app.letstalk.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.app.letstalk.R
import com.app.letstalk.fragments.*
import com.app.letstalk.models.SearchProducts
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.Constants
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Response

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var searchEditText : EditText
    var user_id : Int? = null
    var auth_token : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        searchEditText = findViewById(R.id.search_editText)



        var notification_btn =  findViewById<ImageView>(R.id.notification_btn)
        notification_btn.setOnClickListener {

            supportFragmentManager.beginTransaction().replace(R.id.home_frame, NotificationFragment()).
                addToBackStack(null).commit()

        }


        searchEditText.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->

            Log.d("KEYCODE-----",keyCode.toString())
            Log.d("EVENT----",event.toString())


            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                Log.d("MMMMMMMMMMMMM----",")))))))))")

                return@OnKeyListener true
            }
            false
        })



        searchEditText.setOnTouchListener(object: View.OnTouchListener {
            override fun onTouch(v:View, event: MotionEvent):Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.getAction() === MotionEvent.ACTION_UP)
                {
                    if (event.getRawX() >= (searchEditText.getLeft() - searchEditText.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width()))
//                    {
//                        // your action here
//                        var text = searchEditText.text
//                        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
//                       // val call = service.getSearchResult(text.toString())
//
//
//                        //add prgress bar here
//                        var pBar = findViewById<CrystalPreloader>(R.id.pBar)
//                        pBar.visibility = View.VISIBLE
//                        call.enqueue(object : Callback<Image> {
//                            override fun onResponse(call: Call<Image>, response: Response<Image>) {
//
//                                pBar.visibility = View.GONE
//                                //progressDoalog.dismiss()
////                generateDataList(response.body())
////                Toast.makeText(this@MainActivity, response.body()!![0].title, Toast.LENGTH_SHORT).show()
//                                var responseJson = response.body()
//                                if(responseJson!!.status == true ){
//
//                                    val recycler =findViewById(R.id.search_recycler)as RecyclerView
//                                    recycler.setHasFixedSize(true)
//                                    //recycler_three.isNestedScrollingEnabled = false
//                                    recycler.layoutManager = GridLayoutManager(this@SearchActivity,2)
//                                    recycler.adapter = SeeallAdapter(this@SearchActivity,responseJson.result)
//
//
//                                }
//                                else{
//                                    Toast.makeText(this@SearchActivity, responseJson!!.message, Toast.LENGTH_SHORT).show()
//                                }
//                                Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
//                                //List<RetroPhoto> photoList
//                            }
//
//                            override fun onFailure(call: Call<Image>, t: Throwable) {
//
//                                pBar.visibility = View.GONE
//                                // progressDoalog.dismiss()
//                                Toast.makeText(this@SearchActivity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
//                                Log.d("SADFSDFASDFSDFSDFSD",t.message)
//                            }
//                        })
//                        return true
//                    }

                    {
                        Log.d("MMMMMMMMMMMM","MMMMMMMM")
                        if(searchEditText.text.toString().length > 0){
                            searchProducts(searchEditText.text.toString())
                        }


                    }
                }
                return false
            }
        })








        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        //This class provides a handy way to tie together the functionality of DrawerLayout and the framework ActionBar
        // to implement the recommended design for navigation drawers.
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
        val navHeader = navView.getHeaderView(0)

        //setting drawer layout details
        val image = navHeader.findViewById<ImageView>(R.id.drawerImageView)
        val name = navHeader.findViewById<TextView>(R.id.drawerName)
        val email = navHeader.findViewById<TextView>(R.id.drawerEmail)

        val PREFS_FILENAME = "user_details"
        val prefs = applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
            val url = prefs.getString("image","")

            Log.d("URL=========",Constants.link + url)
           // Picasso.with(this@HomeActivity).load(Constants.link + url).into(image)

            Picasso.with(this@HomeActivity).load(Constants.link + url)
                .resize(150,150)
                .into(image, object: Callback {
                    override fun onSuccess() {
                        val imageBitmap = (image.getDrawable() as BitmapDrawable).getBitmap()
                        val imageDrawable = RoundedBitmapDrawableFactory.create(this@HomeActivity.getResources(), imageBitmap)
                        imageDrawable.setCircular(true)
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f)
                        image.setImageDrawable(imageDrawable)
                    }
                    override fun onError() {
                        //viewHolder.color_image.setImageResource(R.drawable.default_image)
                        //this is called on error
                    }
                })

            name.text = prefs.getString("name","")
            email.text = prefs.getString("email","")


        }


        var cart = findViewById<ImageView>(R.id.cart_btn)
        cart.setOnClickListener {
            supportFragmentManager.beginTransaction().replace(R.id.home_frame,CartFragment()).addToBackStack(null).commit()
        }




        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.home_frame, HomeFragment()).commit()
        }
    }


    fun searchProducts(title:String){






        val PREFS_FILENAME = "user_details"
        val prefs = applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
            //means logged in
            user_id = prefs.getInt("id",0)
            auth_token = prefs.getString("auth_token","")


        }
        else{
            user_id = 0
        }




        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.onSearchProducts(title,user_id.toString(),auth_token!!)

        //add prgress bar here
        var pBar = findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE



        // Check if no view has focus:
//        val view = this.currentFocus
//        view?.let { v ->
//            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
//            imm?.hideSoftInputFromWindow(v.windowToken, 0)
//        }




        call.enqueue(object : retrofit2.Callback<SearchProducts> {


            override fun onResponse(call: Call<SearchProducts>, response: Response<SearchProducts>) {


                val inputMethodManager = getSystemService(
                    Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(
                    getCurrentFocus().getWindowToken(), 0)

                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE

                // Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                searchEditText.text.clear()




                if(responseJson!!.error == false ){

                    // Picasso.with(context).load(Constants.link+responseJson.result.user_image).into(userImage)

                    if(responseJson!!.result.size > 0){
//                        supportFragmentManager
//                            .beginTransaction().
//                                //replace(R.id.home_frame,GlobalProductFragment.newInstance(responseJson)).
//                               replace(R.id.home_frame,GlobalProductFragment.newInstance(responseJson)).
//
//                                addToBackStack(null).commit()


                        var frag: ProductFragment = ProductFragment()
                        var bundle: Bundle = Bundle()
                        bundle.putParcelable("myObject", responseJson)

                        frag.arguments = bundle


                        supportFragmentManager.
                            beginTransaction().
                            replace(R.id.home_frame,frag).addToBackStack(null).
                            commit()

                    }
                   // Toast.makeText(this@HomeActivity, "Hueeeeee", Toast.LENGTH_SHORT).show()


                    else{
                        Toast.makeText(this@HomeActivity, "No Products Found !!", Toast.LENGTH_SHORT).show()
                    }



                }
                else{




                }
               // Log.d("RESPONSE----->>>",responseJson!!.message)
                //Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
                //List<RetroPhoto> photoList
            }

            override fun onFailure(call: Call<SearchProducts>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE

                // progressDoalog.dismiss()
                //pBar.visibility = View.GONE
                Toast.makeText(this@HomeActivity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
       // menuInflater.inflate(R.menu.home, menu)
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun filter(a : String){
        Log.d("FILTER----->>> ",a)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.home -> {

                supportFragmentManager.beginTransaction().replace(R.id.home_frame,HomeFragment()).addToBackStack(null).commit()
            }
            R.id.account-> {
                supportFragmentManager.beginTransaction().replace(R.id.home_frame, ProfileFragment()).addToBackStack(null).commit()
            }
            R.id.notification-> {
                supportFragmentManager.beginTransaction().replace(R.id.home_frame, NotificationFragment()).
                    addToBackStack(null).commit()
            }
            R.id.contact_admin -> {
                supportFragmentManager.beginTransaction().replace(R.id.home_frame,ContactAdminFragment()).addToBackStack(null).commit()
            }
            R.id.my_orders -> {
                supportFragmentManager.beginTransaction().replace(R.id.home_frame,MyOrdersFragment()).addToBackStack(null).commit()
            }
            R.id.privacy_policy -> {
                supportFragmentManager.beginTransaction().replace(R.id.home_frame,PrivacyPolicyFragment()).addToBackStack(null).commit()
            }

            R.id.logout ->{

//                editor.putString("name",responseJson!!.result.name)
//                editor.putString("email",responseJson!!.result.name)
//                editor.putString("image",responseJson!!.result.user_image)
//                editor.putString("auth_token",responseJson!!.result.auth_token)
//                editor.putInt("id",responseJson!!.result.id)
//


                val PREFS_FILENAME = "user_details"
                val prefs = applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                val editor = prefs.edit()
                editor.remove("name")
                editor.remove("email")
                editor.remove("image")
                editor.remove("auth_token")
                editor.remove("id")
                editor.commit()


                val mainIntent = Intent(this@HomeActivity, LoginActivity::class.java)
                this@HomeActivity.startActivity(mainIntent)
                this@HomeActivity.finish()
            }

        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
