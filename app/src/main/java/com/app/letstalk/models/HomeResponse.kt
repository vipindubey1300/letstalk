package com.app.letstalk.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

data class HomeResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : ResultHome
)


data class ResultHome (

    @SerializedName("banners") val banners : List<Banners>,
    @SerializedName("cart_count") val cart_count : Int,
    @SerializedName("category") val category : List<Category>,
    @SerializedName("featured_category") val featured_category : List<Featured_category>,
    @SerializedName("featured_products") val featured_products : List<Products>
)

data class Banners (

    @SerializedName("id") val id : Int,
    @SerializedName("file") val file : String,
    @SerializedName("priority") val priority : Int
)

data class Category (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("priority") val priority : Int,
    @SerializedName("sub_category_count") val sub_category_count : Int,
    @SerializedName("sub_child_category_count") val sub_child_category_count : Int
)

data class Featured_category (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("priority") val priority : Int
)


data class Products (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("sales_price") val sales_price : String,
    @SerializedName("description") val description : String,
    @SerializedName("feature_image") val feature_image : String
)

@Parcelize
data class SearchProducts(
    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : @RawValue List<Products>

):Parcelable