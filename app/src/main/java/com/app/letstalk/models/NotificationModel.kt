package com.app.letstalk.models
import android.app.Notification
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class NotificationModel(



    @SerializedName("result") val result : NotificationCustom
)

data class NotificationCustom(



    @SerializedName("notification_title") val title : String
)