package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class CategoryResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : List<Categories>
)

data class Categories (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("priority") val priority : Int,
    @SerializedName("sub_parent_id") val sub_parent_id : Int,
    @SerializedName("category") val category : List<SubCategories>
)


data class SubCategories (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("priority") val priority : Int,
    @SerializedName("sub_parent_id") val sub_parent_id : Int
)