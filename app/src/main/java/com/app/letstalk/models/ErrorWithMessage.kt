package com.app.letstalk.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ErrorWithMessage(error:Boolean, message:String) {
    @SerializedName("error")
    @Expose
    var error: Boolean

    @SerializedName("message")
    @Expose
    var message: String


    init {
        this.error = error
        this.message = message

    }

}