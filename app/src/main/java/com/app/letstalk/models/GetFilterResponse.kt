package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class GetFilterResponse (

    @SerializedName("error") val error: Boolean,
    @SerializedName("name") val name : String,
    @SerializedName("result") val result : ResultFilter

)

data class ResultFilter (

    @SerializedName("id") val id : Int,
    @SerializedName("min_price") val min_price : String,
    @SerializedName("max_price") val max_price : String,
    @SerializedName("brand") val brand_id : Int,
    @SerializedName("color") val color_id : Int,
    @SerializedName("size") val size_id : Int,
    @SerializedName("brands") val brands : List<Brands>,
    @SerializedName("colors") val colors :List<Colors>,
    @SerializedName("sizes") val sizes : List<Sizes>,
    @SerializedName("tags") val tags : List<Tags>

)



