package com.app.letstalk.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SignUpResponse(error:Boolean, message:String, result:Result) {
    @SerializedName("error")
    @Expose
    var error:Boolean

    @SerializedName("message")
    @Expose
    var message:String

    @SerializedName("result")
    @Expose
    var result:Result


    init{
        this.error = error
        this.message = message
        this.result = result
    }
}

//
//data class Json4Kotlin_Base (
//
//    @SerializedName("result") val result : Result,
//    @SerializedName("error") val error : Boolean,
//    @SerializedName("message") val message : String
//)
 data class Result (

    @SerializedName("first_name") val first_name : String,
    @SerializedName("last_name") val last_name : String,
    @SerializedName("name") val name : String,
    @SerializedName("email") val email : String,
    @SerializedName("user_image") val user_image : String,
    @SerializedName("device_type") val device_type : String,
    @SerializedName("device_token") val device_token : String,
    @SerializedName("agree_terms") val agree_terms : String,
    @SerializedName("roles_id") val roles_id : String,
    @SerializedName("status") val status : String,
    @SerializedName("auth_token") val auth_token : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("id") val id : Int
)