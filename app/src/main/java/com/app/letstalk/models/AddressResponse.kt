package com.app.letstalk.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddressResponse(error:Boolean, result:List<Address>) {
    @SerializedName("error")
    @Expose
    var error:Boolean


    @SerializedName("result")
    @Expose
//    var result:List<Result> = null
    var result:List<Address>
    init{
        this.error = error

        this.result = result
    }
}

class Address(addrId:String, name:String, mobile:String, addrone:String, addrtwo:String , city:String, state:String, country:String, zip:String){

    @SerializedName("id")
    @Expose
    var addrId:String

    @SerializedName("name")
    @Expose
    var name:String

    @SerializedName("mobile")
    @Expose
    var mobile:String

    @SerializedName("address_line")
    @Expose
    var addrone:String

    @SerializedName("address_line2")
    @Expose
    var addrtwo:String

    @SerializedName("city")
    @Expose
    var city:String

    @SerializedName("state")
    @Expose
    var state:String

    @SerializedName("country")
    @Expose
    var country:String

    @SerializedName("zip_code")
    @Expose
    var zip:String


    init {
        this.addrId = addrId
       this.name = name
        this.mobile = mobile
        this.addrone = addrone
        this.addrtwo = addrtwo
        this.city = city
        this.state = state
        this.country = country
        this.zip = zip
    }

}