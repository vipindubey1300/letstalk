package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class ReviewResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("message") val message : String,
    @SerializedName("result") val result : List<Reviews>

)
