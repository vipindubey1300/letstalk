package com.app.letstalk.models

import com.google.gson.annotations.SerializedName


data class CartResponse(

    @SerializedName("error") val error : Boolean,
    @SerializedName("message") val message : String,
    @SerializedName("result") val result : List<ResultCart>
)
data class ResultCart (

    @SerializedName("id") val id : Int,
    @SerializedName("user_id") val user_id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("size_id") val size_id : String,
    @SerializedName("color_id") val color_id : String,
    @SerializedName("brand_id") val brand_id : String,
    @SerializedName("qty") val qty : Int,
    @SerializedName("amount") val amount : Double,
    @SerializedName("total_amount") val total_amount : Double,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("product") val product : Products
)
