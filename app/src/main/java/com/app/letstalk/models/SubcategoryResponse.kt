package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class SubcategoryResponse
    (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : List<SubCategories>
)