package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class ProductInfoResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : ResultProductInfoResponse

)

//t


data class ResultProductInfoResponse (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("slug") val slug : String,
    @SerializedName("parent_category_id") val parent_category_id : Int,
    @SerializedName("sub_category_id") val sub_category_id : Int,
    @SerializedName("category_id") val category_id : Int,
    @SerializedName("price") val price : Double,
    @SerializedName("sales_price") val sales_price : Double,
    @SerializedName("sku_code") val sku_code : Int,
    @SerializedName("buy_status") val buy_status : Boolean,
    @SerializedName("stock_status") val stock_status : Int,
    @SerializedName("quantity") val quantity : Int,
    @SerializedName("quantity_remains") val quantity_remains : Int,
    @SerializedName("weight") val weight : Int,
    @SerializedName("total_review") val total_review : String,
    @SerializedName("avg_rating") val avg_rating : String,
    @SerializedName("weight_unit") val weight_unit : String,
    @SerializedName("size_category_id") val size_category_id : Int,
    @SerializedName("short_description") val short_description : String,
    @SerializedName("description") val description : String,
    @SerializedName("feature_image") val feature_image : String,
    @SerializedName("key_feature") val key_feature_feature : String,
    @SerializedName("environment") val environment : String,
    @SerializedName("reviews") val reviews : List<Reviews>,
    @SerializedName("good_to_know") val good_to_know : String,
    @SerializedName("assembly_info") val assembly_info : String,
    @SerializedName("featured") val featured : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("sub_category") val sub_category : SubCategories,
    @SerializedName("parent_category") val parent_category : Categories,
    @SerializedName("category") val category : Categories,
    @SerializedName("product_colors") val product_colors : List<Product_colors>,
    @SerializedName("product_sizes_category") val product_sizes_category : List<Product_sizes_category>,
    @SerializedName("product_sizes") val product_sizes : List<Product_sizes>,
    @SerializedName("product_tags") val product_tags : List<Product_tags>,
    @SerializedName("product_brands") val product_brands : List<Product_brands>,
    @SerializedName("gallery") val gallery : List<Gallery>
)

data class Reviews (

    @SerializedName("id") val id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("content") val content : String,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("rating") val rating : String,
    @SerializedName("user") val user : CommentUser


)


data class CommentUser (

    @SerializedName("id") val id : Int,
    @SerializedName("user_image") val user_image : String,
    @SerializedName("name") val name : String

)

data class Gallery (

    @SerializedName("id") val id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("image") val image : String

)



data class Product_brands (

    @SerializedName("id") val id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("brand_id") val brand_id : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("brands") val brands : Brands
)
data class Brands (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)


data class Product_colors (

    @SerializedName("id") val id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("color_id") val color_id : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("colors") val colors : Colors
)
data class Colors (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("image") val image : String,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)

data class Product_sizes_category (

    @SerializedName("id") val id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("size_category_id") val size_category_id : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("size_category") val size_category : Size_category
)



data class Size_category (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("description") val description : String,
    @SerializedName("parent_id") val parent_id : Int,
    @SerializedName("priority") val priority : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)


data class Product_sizes (

    @SerializedName("id") val id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("size_id") val size_id : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("sizes") val sizes : Sizes
)

data class Sizes (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("category_id") val category_id : Int,
    @SerializedName("sub_category_id") val sub_category_id : Int,
    @SerializedName("priority") val priority : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)


data class Product_tags (

    @SerializedName("id") val id : Int,
    @SerializedName("tag_id") val tag_id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("tags") val tags : Tags
)

data class Tags (

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)






//data class Parent_category (
//
//    @SerializedName("id") val id : Int,
//    @SerializedName("name") val name : String,
//    @SerializedName("description") val description : String,
//    @SerializedName("image") val image : String,
//    @SerializedName("featured") val featured : Int,
//    @SerializedName("status") val status : Int,
//    @SerializedName("user_id") val user_id : String,
//    @SerializedName("priority") val priority : Int,
//    @SerializedName("parent_id") val parent_id : Int,
//    @SerializedName("sub_parent_id") val sub_parent_id : Int,
//    @SerializedName("created_at") val created_at : String,
//    @SerializedName("updated_at") val updated_at : String
//)

//data class Sub_category (
//
//    @SerializedName("id") val id : Int,
//    @SerializedName("name") val name : String,
//    @SerializedName("description") val description : String,
//    @SerializedName("image") val image : String,
//    @SerializedName("featured") val featured : Int,
//    @SerializedName("status") val status : Int,
//    @SerializedName("user_id") val user_id : Int,
//    @SerializedName("priority") val priority : Int,
//    @SerializedName("parent_id") val parent_id : Int,
//    @SerializedName("sub_parent_id") val sub_parent_id : Int,
//    @SerializedName("created_at") val created_at : String,
//    @SerializedName("updated_at") val updated_at : String
//)