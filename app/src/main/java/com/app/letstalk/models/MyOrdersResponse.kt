package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class MyOrdersResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : List<MyOrders>
)

data class MyOrders (

    @SerializedName("id") val id : Int,
    @SerializedName("user_id") val user_id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("mobile") val mobile : String,
    @SerializedName("address_title") val address_title : String,
    @SerializedName("address_line") val address_line : String,
    @SerializedName("address_line2") val address_line2 : String,
    @SerializedName("street") val street : Int,
    @SerializedName("city") val city : Int,
    @SerializedName("state") val state : Int,
    @SerializedName("country") val country : Int,
    @SerializedName("zip_code") val zip_code : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("payment_status") val payment_status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("order_items") val order_items : List<Order_items>
)


data class Order_items (

    @SerializedName("id") val id : Int,
    @SerializedName("order_id") val order_id : Int,
    @SerializedName("user_id") val user_id : Int,
    @SerializedName("product_id") val product_id : Int,
    @SerializedName("size_id") val size_id : Int,
    @SerializedName("color_id") val color_id : Int,
    @SerializedName("brand_id") val brand_id : String,
    @SerializedName("qty") val qty : Int,
    @SerializedName("amount") val amount : Double,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("product") val products : Products
)