package com.app.letstalk.models

import com.google.gson.annotations.SerializedName

data class NotificationResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : ResultNotification
)

data class ResultNotification (

    @SerializedName("total") val total : Int,
    @SerializedName("notifications") val notifications : List<Notifications>
)

data class Notifications (

    @SerializedName("id") val id : Int,
    @SerializedName("sender") val sender : Int,
    @SerializedName("user_id") val user_id : Int,
    @SerializedName("error") val error : Boolean,
    @SerializedName("notification_title") val notification_title : String,
    @SerializedName("data") val data : String,
    @SerializedName("type") val type : Int,
    @SerializedName("status") val status : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String
)