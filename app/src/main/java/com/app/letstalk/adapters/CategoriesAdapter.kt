package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.ChildCategories
import com.app.letstalk.fragments.SubCategoriesFragment
import com.app.letstalk.models.Categories
import com.app.letstalk.models.Category

import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso
import java.util.*

class CategoriesAdapter(mcontext: Context, list: ArrayList<Categories>) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<Categories>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CategoriesAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.categories_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: CategoriesAdapter.ViewHolder, i: Int) {


        viewHolder.name.setText(list.get(i).name)


        val categoryRecycler =  viewHolder.recyclerSub
        categoryRecycler.layoutManager = LinearLayoutManager(mcontext, RecyclerView.HORIZONTAL ,false)
        categoryRecycler.adapter = SubcategoryAdapter(mcontext, ArrayList(list.get(i).category))


        viewHolder.view_all.setOnClickListener {

            Log.d("ID",list.get(i).id.toString())
            Log.d("NAME",list.get(i).name.toString())

            var sub_frag: ChildCategories = ChildCategories()
            var bundle: Bundle = Bundle()
            bundle.putString("category_name",list.get(i).name)
            bundle.putInt("category_id",list.get(i).id)
            sub_frag.arguments = bundle

            var context = mcontext as AppCompatActivity

            context.supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                .replace(R.id.home_frame, sub_frag).addToBackStack(null).commit()
        }








    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



        var name : TextView
        var recyclerSub : RecyclerView

        var view_all : TextView



        init {

            name = itemView.findViewById(R.id.category_name)

            recyclerSub = itemView.findViewById(R.id.sub_category_recycler)
            view_all = itemView.findViewById(R.id.view_all)


        }




    }

}