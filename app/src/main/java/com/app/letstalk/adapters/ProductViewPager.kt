package com.app.letstalk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.app.letstalk.R
import com.app.letstalk.models.Gallery
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class ProductViewPager(mcontext: Context, list: ArrayList<Gallery>) : PagerAdapter(){

    var mcontext: Context
    var list: ArrayList<Gallery>


    private lateinit var layoutInflater: LayoutInflater


    init {
        this.mcontext = mcontext
        this.list = list

    }
//    private val images = arrayOf<Int>(R.drawable.viewpage, R.drawable.boy, R.drawable.my_order)
//

    val images = arrayOf<Int>(R.drawable.viewpage, R.drawable.boy, R.drawable.my_order)

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater



        val view = layoutInflater.inflate(R.layout.viewpager_home, null)
        val imageView = view.findViewById(R.id.imageViewViewpager) as ImageView
        //imageView.setImageResource(images[position])

        Picasso.with(mcontext).load(Constants.link +list.get(position).image).into(imageView)

        val vp = container as ViewPager
        vp.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return list.size
    }
}