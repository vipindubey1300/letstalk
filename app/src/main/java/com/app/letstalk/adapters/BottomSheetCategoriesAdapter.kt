package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.BottomSheet
import com.app.letstalk.fragments.CategoriesFragment
import com.app.letstalk.fragments.ProductFragment
import com.app.letstalk.fragments.SubCategoriesFragment
import com.app.letstalk.models.Category
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class BottomSheetCategoriesAdapter(mcontext: Context, list: ArrayList<Category>,bottomSheet:BottomSheet) : RecyclerView.Adapter<BottomSheetCategoriesAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<Category>
    var bottomSheet : BottomSheet


    init {
        this.mcontext = mcontext
        this.list = list
        this.bottomSheet = bottomSheet

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BottomSheetCategoriesAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.bottom_sheet_category_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: BottomSheetCategoriesAdapter.ViewHolder, i: Int) {

        viewHolder.name.setText(list.get(i).name)
        Picasso.with(mcontext).load(Constants.link +list.get(i).image).into(viewHolder.image)
        viewHolder.main_layout.setOnClickListener {

            if(list.get(i).sub_category_count == 0 ){
                //directly products page
                Log.d("PATH------------","tier one")

                var prod_frag: ProductFragment = ProductFragment()
                var bundle: Bundle = Bundle()
                bundle.putString("category_name",list.get(i).name)
                bundle.putInt("category_id",list.get(i).id)
                prod_frag.arguments = bundle

                var context = mcontext as AppCompatActivity

                context.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.home_frame, prod_frag).addToBackStack(null).commit()
            }
            else if(list.get(i).sub_category_count > 0
                && list.get(i).sub_child_category_count <= list.get(i).sub_category_count ){

                //means 2 tier category details
                Log.d("PATH------------","tier two")

                var sub_frag: SubCategoriesFragment = SubCategoriesFragment()
                var bundle: Bundle = Bundle()
                bundle.putString("category_name",list.get(i).name)
                bundle.putInt("category_id",list.get(i).id)
                sub_frag.arguments = bundle

                var context = mcontext as AppCompatActivity

                context.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.home_frame, sub_frag).addToBackStack(null).commit()

            }
            else if (list.get(i).sub_category_count > 0
                && list.get(i).sub_category_count < list.get(i).sub_child_category_count  ){
                //means 3 tier category

                Log.d("PATH------------","tier three")

                var category_frag: CategoriesFragment = CategoriesFragment ()
                var bundle: Bundle = Bundle()
                bundle.putString("category_name",list.get(i).name)
                bundle.putInt("category_id",list.get(i).id)
                category_frag.arguments = bundle

                var context = mcontext as AppCompatActivity

                context.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.home_frame, category_frag).addToBackStack(null).commit()

            }

            bottomSheet.dismiss()

        }


    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0

        Log.d("SIZEEEEEEEEE---------",list.size.toString())
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var image : ImageView
        var name : TextView
        var main_layout :RelativeLayout




        init {
            image = itemView.findViewById(R.id.category_image)
            name = itemView.findViewById(R.id.category_name)
            main_layout = itemView.findViewById(R.id.main_layout)



        }

    }

}