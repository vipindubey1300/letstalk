package com.app.letstalk.adapters

import android.content.Context
import android.os.Build
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Product_sizes
import com.app.letstalk.models.Sizes
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class SizeAdapter(mcontext: Context, list: ArrayList<Product_sizes> ,
                  callback : SizeSelect) : RecyclerView.Adapter<SizeAdapter.ViewHolder>() {

    var checkedPosition = -1

    var mcontext: Context
    var list: ArrayList<Product_sizes>
    var callback : SizeSelect


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.sizes_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

       viewHolder.size_text.text = list.get(i).sizes.name
        if(checkedPosition == i){
            viewHolder.main_card.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.lightYellow)
        }
        else{
            viewHolder.main_card.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.white)
        }

        viewHolder.main_card.setOnClickListener {
            viewHolder.bind(list[i].sizes,i)
        }



    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var size_text : TextView
        var main_card  : LinearLayout



        init {
            size_text = itemView.findViewById(R.id.size_text)
            main_card = itemView.findViewById(R.id.main_layout)



        }

        fun bind(sizes: Sizes, position:Int){


            val result = callback.onSizeSelect(list.get(position).size_id,position)


            Log.d("RESut-----",result.toString())

            if(result == 0){
                // color_image.setColorFilter( R.color.lightYellow)
                checkedPosition = position
                notifyDataSetChanged()
            }
            else if(result == 1){
                //color_image.clearColorFilter()
                checkedPosition = -1
                notifyDataSetChanged()
            }
            else{
                for( i in 0..itemCount -1){

                    //  Log.d("i value-",""+position)

                    //color_image.clearColorFilter()


                    if(i == position){
                        checkedPosition = position
                        notifyDataSetChanged()



                    }








                }
            }
        }




    }



}
interface SizeSelect{
    fun onSizeSelect(sizeSelected: Int,position: Int) : Int
}
