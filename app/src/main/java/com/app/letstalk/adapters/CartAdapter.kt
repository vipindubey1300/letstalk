package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.ProductFragment
import com.app.letstalk.fragments.ProductInfoFragment
import com.app.letstalk.models.Category

import com.app.letstalk.models.Products
import com.app.letstalk.models.ResultCart
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class CartAdapter(mcontext: Context, list: ArrayList<ResultCart>,callback :CartRemove) : RecyclerView.Adapter<CartAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<ResultCart>
    var callback : CartRemove


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CartAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.cart_item_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: CartAdapter.ViewHolder, i: Int) {

        viewHolder.name.setText(list.get(i).product.name)
        Picasso.with(mcontext).load(Constants.link +list.get(i).product.feature_image).into(viewHolder.image)

        viewHolder.price.setText("$ " + list.get(i).product.sales_price)

        viewHolder.description.setText(HtmlCompat.fromHtml(list.get(i).product.description.trim(), HtmlCompat.FROM_HTML_MODE_COMPACT))


        viewHolder.remove_btn.setOnClickListener {
            callback.onremoveCart(list.get(i).id,i)
        }
        viewHolder.qty.setText("Quantity : "+list.get(i).qty.toString() )




    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var image : ImageView
        var qty : TextView
        var name : TextView
        var description : TextView
        var price : TextView
        var product_main_layout : LinearLayout
        var remove_btn : ImageView




        init {
            qty = itemView.findViewById(R.id.qty)
            image = itemView.findViewById(R.id.product_image)
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)
            price = itemView.findViewById(R.id.price)
            product_main_layout = itemView.findViewById(R.id.product_main_layout)
            remove_btn = itemView.findViewById(R.id.remove_btn)



        }




    }

}

interface CartRemove{
    fun onremoveCart(cart : Int ,position: Int)
}