package com.app.letstalk.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.models.Address
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.FieldPosition

class AddressAdapterCheckout(mcontext: Context, list: ArrayList<Address>,callback :AddressRadio) : RecyclerView.Adapter<AddressAdapterCheckout.ViewHolder>() {

    var checkedPosition = -1
    var mcontext: Context
    var list: ArrayList<Address>
    var callback :AddressRadio


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.address_card_checkout, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {


        var name = list.get(i).name
        viewHolder.name.setText(name)

        viewHolder.phone.setText(list.get(i).mobile)

        val fulladdr = list.get(i).addrone + " " + list.get(i).city + " " +list.get(i).state

        viewHolder.fullAddr.setText(fulladdr)
        if(checkedPosition == i){
            viewHolder.radio.isChecked = true
        }
        else{
            viewHolder.radio.isChecked = false
        }

        viewHolder.radio.setOnClickListener {
            viewHolder.bind(list[i].addrId.toInt(),i)
        }







    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {



        var name : TextView
        var phone : TextView
        var fullAddr : TextView
        var radio : RadioButton



        init {

            name = itemView.findViewById(R.id.addr_name)
            phone = itemView.findViewById(R.id.addr_phone)
            fullAddr = itemView.findViewById(R.id.addr_full)
            radio = itemView.findViewById(R.id.radio)


        }

        fun bind(id: Int, position:Int){

            val result = callback.onCheckAddress(id,position)


            Log.d("RESut-----",result.toString())

            if(result == true){
                // color_image.setColorFilter( R.color.lightYellow)
                checkedPosition = position
                notifyDataSetChanged()
            }


        }




    }



}

interface AddressRadio{
    fun onCheckAddress(addr: Int,position: Int) : Boolean
}
