package com.app.letstalk.adapters

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Product_sizes
import com.app.letstalk.models.Sizes
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class SizeAdapFilter(mcontext: Context, list: ArrayList<Sizes>,callback: SizeSelectFilter) : RecyclerView.Adapter<SizeAdapFilter.ViewHolder>() {


    var mcontext: Context
    var list: ArrayList<Sizes>
    var callback: SizeSelectFilter


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.sizes_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        viewHolder.size_text.text = list.get(i).name

        viewHolder.mainLayout.setOnClickListener {


            var result = callback.onSizeSelectFilter(list.get(i).id)
            if(result){
                viewHolder.mainLayout.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.lightYellow)
                this@SizeAdapFilter.notifyDataSetChanged()
            }
            else{
                viewHolder.mainLayout.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.white)
                this@SizeAdapFilter.notifyDataSetChanged()
            }
        }
    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var size_text : TextView
        var mainLayout : LinearLayout


        init {
            size_text = itemView.findViewById(R.id.size_text)
            mainLayout = itemView.findViewById(R.id.main_layout)



        }




    }



}

interface SizeSelectFilter{
    fun onSizeSelectFilter(sizeSelected: Int) : Boolean
}