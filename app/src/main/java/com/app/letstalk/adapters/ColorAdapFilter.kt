package com.app.letstalk.adapters

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Colors
import com.app.letstalk.models.Product_colors
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class ColorAdapFilter(mcontext: Context, list: ArrayList<Colors>,callback: ColorSelectFilter) : RecyclerView.Adapter<ColorAdapFilter.ViewHolder>() {


    var mcontext: Context
    var list: ArrayList<Colors>
    var callback: ColorSelectFilter


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.color_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {


        Picasso.with(mcontext).load(Constants.link +list.get(i).image)
            .resize(150,150)
            .into(viewHolder.color_image, object: Callback {
                override fun onSuccess() {
                    val imageBitmap = (viewHolder.color_image.getDrawable() as BitmapDrawable).getBitmap()
                    val imageDrawable = RoundedBitmapDrawableFactory.create(mcontext.getResources(), imageBitmap)
                    imageDrawable.setCircular(true)
                    imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f)
                    viewHolder.color_image.setImageDrawable(imageDrawable)
                }
                override fun onError() {
                    //viewHolder.color_image.setImageResource(R.drawable.default_image)
                }
            })

        viewHolder.color_image.setOnClickListener {
            var result = callback.onColorSelectFilter(list.get(i).id)
            if(result){
                viewHolder.color_image.setColorFilter( R.color.lightYellow)
                this@ColorAdapFilter.notifyDataSetChanged()
            }
            else{
                viewHolder.color_image.clearColorFilter()
                this@ColorAdapFilter.notifyDataSetChanged()
            }
        }
    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var color_image : ImageView



        init {
            color_image = itemView.findViewById(R.id.color_image)



        }




    }



}

interface ColorSelectFilter{
    fun onColorSelectFilter(colorSelected: Int) : Boolean
}