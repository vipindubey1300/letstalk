package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.CategoriesFragment
import com.app.letstalk.fragments.ProductFragment
import com.app.letstalk.fragments.SubCategoriesFragment
import com.app.letstalk.models.Category
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso
import java.util.*

class HomeCategoriesAdapter(mcontext: Context, list: ArrayList<Category>) : RecyclerView.Adapter<HomeCategoriesAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<Category>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): HomeCategoriesAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.home_categories_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: HomeCategoriesAdapter.ViewHolder, i: Int) {
        //0...light gree
        //1 ....light red
        //2...light grey
        //3 ...light yeallw

        viewHolder.name.setText(list.get(i).name)
        Picasso.with(mcontext).load(Constants.link +list.get(i).image).into(viewHolder.image)
        val rnds = (0..3).random()

        when(rnds){
            0 -> viewHolder.background.setCardBackgroundColor(mcontext.resources.getColor(R.color.lightGreen))
            1 -> viewHolder.background.setCardBackgroundColor(mcontext.resources.getColor(R.color.lightRed))
            2 -> viewHolder.background.setCardBackgroundColor(mcontext.resources.getColor(R.color.grey))
            3 -> viewHolder.background.setCardBackgroundColor(mcontext.resources.getColor(R.color.lightYellow))
        }



        viewHolder.image.setOnClickListener {



            if(list.get(i).sub_category_count == 0 ){
                //directly products page
                Log.d("PATH------------","tier one")

                var prod_frag: ProductFragment = ProductFragment()
                var bundle: Bundle = Bundle()
                bundle.putString("category_name",list.get(i).name)
                bundle.putInt("category_id",list.get(i).id)
                prod_frag.arguments = bundle

                var context = mcontext as AppCompatActivity

                context.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.home_frame, prod_frag).addToBackStack(null).commit()
            }
            else if(list.get(i).sub_category_count > 0
                && list.get(i).sub_child_category_count <= list.get(i).sub_category_count ){

                //means 2 tier category details
                Log.d("PATH------------","tier two")

                var sub_frag: SubCategoriesFragment = SubCategoriesFragment()
                var bundle: Bundle = Bundle()
                bundle.putString("category_name",list.get(i).name)
                bundle.putInt("category_id",list.get(i).id)
                sub_frag.arguments = bundle

                var context = mcontext as AppCompatActivity

                context.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.home_frame, sub_frag).addToBackStack(null).commit()

            }
            else if (list.get(i).sub_category_count > 0
                && list.get(i).sub_category_count < list.get(i).sub_child_category_count  ){
                //means 3 tier category

                Log.d("PATH------------","tier three")

                var category_frag: CategoriesFragment= CategoriesFragment ()
                var bundle: Bundle = Bundle()
                bundle.putString("category_name",list.get(i).name)
                bundle.putInt("category_id",list.get(i).id)
                category_frag.arguments = bundle

                var context = mcontext as AppCompatActivity

                context.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                    .replace(R.id.home_frame, category_frag).addToBackStack(null).commit()

            }




        }



    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var image : ImageView
        var name : TextView
        var background : CardView




        init {
            image = itemView.findViewById(R.id.category_image)
            name = itemView.findViewById(R.id.category_name)

            background = itemView.findViewById(R.id.background_layout)


        }




    }

}