package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.ProductFragment
import com.app.letstalk.fragments.ProductInfoFragment
import com.app.letstalk.models.Category
import com.app.letstalk.models.MyOrders

import com.app.letstalk.models.Products
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class MyOrdersAdapter(mcontext: Context, list: ArrayList<MyOrders>) : RecyclerView.Adapter<MyOrdersAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<MyOrders>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyOrdersAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.product_card_my_orders, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: MyOrdersAdapter.ViewHolder, i: Int) {

        viewHolder.name.setText(list.get(i).order_items.get(0).products.name)


        Picasso.with(mcontext).load(Constants.link +list.get(i).order_items.get(0).products.feature_image).into(viewHolder.image)

        viewHolder.price.setText("$ " + list.get(i).order_items.get(0).products.sales_price)

        viewHolder.description.setText(HtmlCompat.fromHtml(list.get(i).order_items.get(0).products.description.trim(), HtmlCompat.FROM_HTML_MODE_COMPACT))

        if(list.get(i).status == 0){
            viewHolder.status.setText("In Transit")
        }


        viewHolder.product_main_layout.setOnClickListener {

            var prod_frag: ProductInfoFragment = ProductInfoFragment()
            var bundle: Bundle = Bundle()
            bundle.putInt("product_id",list.get(i).order_items.get(0).product_id)
            prod_frag.arguments = bundle

            var context = mcontext as AppCompatActivity

            context.supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                .replace(R.id.home_frame, prod_frag).addToBackStack(null).commit()
        }




    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var image : ImageView
        var name : TextView
        var status : TextView
        var description : TextView
        var price : TextView
        var product_main_layout : LinearLayout




        init {
            image = itemView.findViewById(R.id.product_image)
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)
            price = itemView.findViewById(R.id.price)
            product_main_layout = itemView.findViewById(R.id.product_main_layout)
            status = itemView.findViewById(R.id.status_text)



        }




    }

}