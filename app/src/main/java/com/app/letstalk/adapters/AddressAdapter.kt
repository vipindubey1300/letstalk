package com.app.letstalk.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.models.Address
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.FieldPosition

class AddressAdapter(mcontext: Context, list: ArrayList<Address>,callback : AddressRemove) : RecyclerView.Adapter<AddressAdapter.ViewHolder>() {


    var mcontext: Context
    var list: ArrayList<Address>
    var callback : AddressRemove

    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.address_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {


        var name = list.get(i).name
        viewHolder.name.setText(name)

        viewHolder.phone.setText(list.get(i).mobile)

        val fulladdr = list.get(i).addrone + " " + list.get(i).city + " " +list.get(i).state

        viewHolder.fullAddr.setText(fulladdr)



        viewHolder.removeBtn.setOnClickListener {


            // var addrId = list.get(i).addrId
            callback.onremoveAddress(list.get(i),i)

        }



    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var removeBtn : ImageView
        var name : TextView
        var phone : TextView
        var fullAddr : TextView



        init {
            removeBtn = itemView.findViewById(R.id.remove_addr)
            name = itemView.findViewById(R.id.addr_name)
            phone = itemView.findViewById(R.id.addr_phone)
            fullAddr = itemView.findViewById(R.id.addr_full)


        }




    }



}

interface AddressRemove{
    fun onremoveAddress(addr: Address,position: Int)
}