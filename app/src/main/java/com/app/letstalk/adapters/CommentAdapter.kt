package com.app.letstalk.adapters

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Reviews
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class CommentAdapter(mcontext: Context, list: ArrayList<Reviews>) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {


    var mcontext: Context
    var list: ArrayList<Reviews>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.comment_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val obj = list.get(i)

        viewHolder.comment.setText(obj.content)
        viewHolder.userName.setText(obj.user.name)
        viewHolder.date.setText(obj.created_at.toString().substring(0,10))

        viewHolder.userRating.rating = obj.rating.toFloat()



                Picasso.with(mcontext).load(Constants.link+obj.user.user_image)
            .resize(50,50)
            .into(viewHolder.userImage, object: com.squareup.picasso.Callback {
                override fun onSuccess() {
                    val imageBitmap = (viewHolder.userImage.getDrawable() as BitmapDrawable).getBitmap()
                    val imageDrawable = RoundedBitmapDrawableFactory.create(mcontext!!.getResources(), imageBitmap)
                    imageDrawable.setCircular(true)
                    imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f)
                    viewHolder.userImage.setImageDrawable(imageDrawable)
                }
                override fun onError() {
                    //viewHolder.color_image.setImageResource(R.drawable.default_image)
                }
            })





    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    //this will be viewholder for the view mtlb where to put context
    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var userName: TextView
        var date: TextView
        var userImage: ImageView
        var comment: TextView
        var userRating = RatingBar(mcontext)



        init {
            userName = itemView.findViewById(R.id.userName)

            userImage = itemView.findViewById(R.id.userImage)
            userRating  = itemView.findViewById(R.id.userRating)
            date  = itemView.findViewById(R.id.date)
            comment  = itemView.findViewById(R.id.comment)



        }




    }
}