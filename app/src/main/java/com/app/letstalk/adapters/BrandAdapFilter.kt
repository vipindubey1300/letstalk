package com.app.letstalk.adapters

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Brands
import com.app.letstalk.models.Colors
import com.app.letstalk.models.Product_colors
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso




class BrandAdapFilter(mcontext: Context, list: ArrayList<Brands>,callback:BrandSelectFilter) : RecyclerView.Adapter<BrandAdapFilter.ViewHolder>() {


    var mcontext: Context
    var list: ArrayList<Brands>
    var callback : BrandSelectFilter


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.brand_filter_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {



        val displayMetrics =  mcontext.getResources().getDisplayMetrics()
        val width = displayMetrics.widthPixels
        var height = displayMetrics.heightPixels

//        val homeLayoutsparams = viewHolder.mainLayout.getLayoutParams()
//        homeLayoutsparams.width = width / 3



        viewHolder.mainLayout.setOnClickListener {
            //viewHolder.mainLayout.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.lightYellow)

            var result = callback.onBrandSelectFilter(list.get(i).id,i)
            if(result){
                viewHolder.mainLayout.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.lightYellow)
                this@BrandAdapFilter.notifyDataSetChanged()
            }
            else{
                viewHolder.mainLayout.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.white)
                this@BrandAdapFilter.notifyDataSetChanged()
            }
        }


        viewHolder.name.text = list.get(i).name
    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var mainLayout : RelativeLayout
        var name : TextView



        init {
            mainLayout = itemView.findViewById(R.id.main_layout)
            name = itemView.findViewById(R.id.brand_text)



        }




    }



}
interface BrandSelectFilter{
    fun onBrandSelectFilter(brandSelected: Int,position: Int) : Boolean
}
