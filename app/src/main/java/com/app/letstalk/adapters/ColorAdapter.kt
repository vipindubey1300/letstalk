package com.app.letstalk.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Colors
import com.app.letstalk.models.Product_colors
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class ColorAdapter(mcontext: Context, list: ArrayList<Product_colors> ,
                   callback : ColorSelect) : RecyclerView.Adapter<ColorAdapter.ViewHolder>() {

    var selected = false
    var checkedPosition = -1


    var mcontext: Context
    var list: ArrayList<Product_colors>
    var callback : ColorSelect


    init {
        this.mcontext = mcontext
        this.list = list
        this.callback = callback

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.color_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        if(checkedPosition == i){
            viewHolder.color_image.setColorFilter( R.color.lightYellow)
        }
        else{
            viewHolder.color_image.clearColorFilter()
        }

       viewHolder.main_card.setOnClickListener {
           viewHolder.bind(list[i].colors,i)
       }


//        viewHolder.main_card.setOnClickListener {
//
//
//
//            viewHolder.main_card.backgroundTintList = ContextCompat.getColorStateList(mcontext, R.color.lightYellow)
//
//
//            if(selected){
//                //means already selected
//
//                if(position == list.get(i).color_id) {
//                    viewHolder.color_image.clearColorFilter()
//
//                }
//
//
//            }
//            else{
//                val res = callback.onColorSelect(list.get(i).color_id)
//                if(res){
//                    viewHolder.color_image.setColorFilter( R.color.lightYellow)
//                    position = list.get(i).color_id
//                    selected = true
//
//                }
//                else{
//                    viewHolder.color_image.clearColorFilter()
//                }
//            }
//
//        }


        Picasso.with(mcontext).load(Constants.link +list.get(i).colors.image)
            .resize(150,150)
            .into(viewHolder.color_image, object: Callback {
                override fun onSuccess() {
                    val imageBitmap = (viewHolder.color_image.getDrawable() as BitmapDrawable).getBitmap()
                    val imageDrawable = RoundedBitmapDrawableFactory.create(mcontext.getResources(), imageBitmap)
                    imageDrawable.setCircular(true)
                    imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f)
                    viewHolder.color_image.setImageDrawable(imageDrawable)
                }
                override fun onError() {
                    //viewHolder.color_image.setImageResource(R.drawable.default_image)
                }
            })

       // Picasso.with(mcontext).load(Constants.link +list.get(i).colors.image).into(viewHolder.color_image)
    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var color_image : ImageView
        var main_card : CardView



        init {
            color_image = itemView.findViewById(R.id.color_image)
            main_card = itemView.findViewById(R.id.main_card)


        }



        fun bind(colors: Colors, position:Int){
//            if(checkedPosition  == -1){
//                color_image.setColorFilter( R.color.lightYellow)
//                checkedPosition = position
//            }
//            else{
//                if(checkedPosition == adapterPosition){
//                    color_image.clearColorFilter()
//                    checkedPosition = -1
//                }
//                else{
//
//                    for( i in ArrayList<Int>(itemCount)){
//                        if(checkedPosition == position){
//                            color_image.clearColorFilter()
//                            checkedPosition = -1
//                        }
//                        else{
//
//                            color_image.setColorFilter( R.color.lightYellow)
//                            checkedPosition = position
//                        }
//                    }
//
//                }
//            }

            val result = callback.onColorSelect(list.get(position).color_id,position)


            Log.d("RESut-----",result.toString())

            if(result == 0){
               // color_image.setColorFilter( R.color.lightYellow)
                checkedPosition = position
                notifyDataSetChanged()
            }
            else if(result == 1){
                //color_image.clearColorFilter()
                checkedPosition = -1
                notifyDataSetChanged()
            }
            else{
                 for( i in 0..itemCount -1){

                   //  Log.d("i value-",""+position)

                     //color_image.clearColorFilter()


                     if(i == position){
                         checkedPosition = position
                         notifyDataSetChanged()
                         Log.d("CHANGE--------",checkedPosition.toString())
                         //color_image.setColorFilter(R.color.lightYellow)


                     }








                 }
            }
        }




    }



}
interface ColorSelect{
    fun onColorSelect(colorSelected: Int,position: Int) : Int
}
