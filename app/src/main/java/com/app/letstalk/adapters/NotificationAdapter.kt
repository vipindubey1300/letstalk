package com.app.letstalk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.models.Notifications

class NotificationAdapter(mcontext: Context, list: ArrayList<Notifications>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {


    var mcontext: Context
    var list:  ArrayList<Notifications>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.notification_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val obj = list.get(i)

        viewHolder.heading.setText(obj.notification_title)
        viewHolder.time.setText(obj.updated_at)





    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    //this will be viewholder for the view mtlb where to put context
    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


       var heading: TextView
        var time: TextView



        init {
           heading = itemView.findViewById(R.id.heading)


            time  = itemView.findViewById(R.id.time)


        }




    }
}