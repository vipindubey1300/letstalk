package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.ProductFragment
import com.app.letstalk.models.Category

import com.app.letstalk.models.SubCategories
import com.app.letstalk.models.SubcategoryResponse
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso
import java.util.*

class SubcategoryAdapter(mcontext: Context, list: ArrayList<SubCategories>) : RecyclerView.Adapter<SubcategoryAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<SubCategories>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SubcategoryAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.subcategory_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: SubcategoryAdapter.ViewHolder, i: Int) {


        viewHolder.name.setText(list.get(i).name)
        Picasso.with(mcontext).load(Constants.link +list.get(i).image).into(viewHolder.image)

        val displayMetrics =  mcontext.getResources().getDisplayMetrics()
       // mcontext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)


        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var w = width * 0.4



        val params: ViewGroup.LayoutParams = viewHolder.layout.layoutParams
        params.width = w.toInt()
        viewHolder.layout.layoutParams = params



        viewHolder.image.setOnClickListener {
            var prod_frag: ProductFragment = ProductFragment()
            var bundle: Bundle = Bundle()
            bundle.putString("category_name",list.get(i).name)
            bundle.putInt("category_id",list.get(i).id)
            prod_frag.arguments = bundle

            var context = mcontext as AppCompatActivity

            context.supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                .replace(R.id.home_frame, prod_frag).addToBackStack(null).commit()
        }


    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var image : ImageView
        var name : TextView
        var layout : LinearLayout




        init {
            name = itemView.findViewById(R.id.sub_category_name)
            image = itemView.findViewById(R.id.sub_category_image)
            layout = itemView.findViewById(R.id.parent_layout)




        }




    }

}