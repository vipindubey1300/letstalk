package com.app.letstalk.adapters

import com.app.letstalk.models.Products
import com.google.gson.annotations.SerializedName

data class ProductsResponse (

    @SerializedName("error") val error : Boolean,
    @SerializedName("result") val result : List<Products>
)
