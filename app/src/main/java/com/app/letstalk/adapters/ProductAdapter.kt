package com.app.letstalk.adapters

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.R
import com.app.letstalk.fragments.ProductFragment
import com.app.letstalk.fragments.ProductInfoFragment
import com.app.letstalk.models.Category

import com.app.letstalk.models.Products
import com.app.letstalk.utils.Constants
import com.squareup.picasso.Picasso

class ProductAdapter(mcontext: Context, list: ArrayList<Products>) : RecyclerView.Adapter<ProductAdapter.ViewHolder>(){
    var mcontext: Context
    var list: ArrayList<Products>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ProductAdapter.ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.product_card, viewGroup, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(viewHolder: ProductAdapter.ViewHolder, i: Int) {

        viewHolder.name.setText(list.get(i).name)
        Picasso.with(mcontext).load(Constants.link +list.get(i).feature_image).into(viewHolder.image)

        viewHolder.price.setText("$ " + list.get(i).sales_price)

       viewHolder.description.setText(HtmlCompat.fromHtml(list.get(i).description.trim(), HtmlCompat.FROM_HTML_MODE_COMPACT))

//        var prod_frag: ProductInfoFragment = ProductInfoFragment()
//        var bundle: Bundle = Bundle()
//        bundle.putInt("product_id",list.get(i).id)
//        prod_frag.arguments = bundle
//
//        var context = mcontext as AppCompatActivity
//
//        context.supportFragmentManager.beginTransaction()
//            .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
//            .replace(R.id.home_frame, prod_frag).addToBackStack(null).commit()

        viewHolder.product_main_layout.setOnClickListener {

            var prod_frag: ProductInfoFragment = ProductInfoFragment()
        var bundle: Bundle = Bundle()
        bundle.putInt("product_id",list.get(i).id)
        prod_frag.arguments = bundle

        var context = mcontext as AppCompatActivity

        context.supportFragmentManager.beginTransaction()
            .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
            .replace(R.id.home_frame, prod_frag).addToBackStack(null).commit()
        }


    }


    override fun getItemCount(): Int {
        return if (null != list) list.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var image : ImageView
        var name : TextView
        var description : TextView
        var price : TextView
        var product_main_layout : LinearLayout




        init {
            image = itemView.findViewById(R.id.product_image)
            name = itemView.findViewById(R.id.name)
            description = itemView.findViewById(R.id.description)
            price = itemView.findViewById(R.id.price)
            product_main_layout = itemView.findViewById(R.id.product_main_layout)



        }




    }

}