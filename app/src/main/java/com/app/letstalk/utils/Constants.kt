package com.app.letstalk.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class Constants {
    companion object {


        var link: String = "http://webmobril.org/dev/letstalk/"
        //var link: String = "http://192.168.0.107/"

        public fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
            return if (connectivityManager is ConnectivityManager) {
                val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
                networkInfo?.isConnected ?: false
            } else false
        }
    }

}