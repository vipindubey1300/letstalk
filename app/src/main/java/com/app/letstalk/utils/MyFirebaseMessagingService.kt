package com.app.letstalk.utils

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.google.firebase.messaging.FirebaseMessagingService

import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.app.letstalk.models.NotificationResponse
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.app.letstalk.models.NotificationModel


class MyFirebaseMessagingService:FirebaseMessagingService() {
    private var notifManager: NotificationManager? = null
    private  var mChannel: NotificationChannel? = null

    var token : String? = null
    private val notificationIcon:Int




        get() {
            val useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return if (useWhiteIcon) R.drawable.applogo else R.drawable.applogo
        }
    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage:RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.e("MyFirebaseMessagingService", "Called")
        Log.e("MyFirebaseMessagingService", remoteMessage.getFrom())
        val body = remoteMessage.getData().toString()
        Log.e("Notification Response--", body)
        val gson = Gson()
        //NotificationResponse response = gson.fromJson(body, NotificationResponse.class);
        val result = gson.fromJson(body, NotificationModel::class.java)



        Log.e("Notification After--", result.result.title)
        displayNotification(body,result.result.title)
    }
    override fun onNewToken(s:String) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s)

        token = s
       // LoginPre.getActiveInstance(getApplicationContext()).setDeviceToken(s)
    }
    private fun displayNotification(body:String,title:String) {
        val intent = Intent(this, HomeActivity::class.java)
       // val title = body.get(0).
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        if (notifManager == null)
        {
            notifManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val builder: NotificationCompat.Builder
            val pendingIntent: PendingIntent
            val importance = NotificationManager.IMPORTANCE_HIGH
            if (mChannel == null)
            {
                mChannel = NotificationChannel("0", "Lets Talk", importance)
                // mChannel.setDescription(message);//
                mChannel?.enableVibration(true)
                notifManager?.createNotificationChannel(mChannel)
            }
            builder = NotificationCompat.Builder(this, "0")
            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT)
            builder.setContentTitle(title)
                .setContentText("Lets talk")
                .setSmallIcon(notificationIcon) // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            val notification = builder.build()
            notifManager?.notify(0, notification)
        }
        else
        {
            // pendingIntent:PendingIntent = null
            var pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT)
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText("LetsTalk")
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
                .setSound(defaultSoundUri)
                .setSmallIcon(notificationIcon)
                .setContentIntent(pendingIntent)
                .setStyle(NotificationCompat.BigTextStyle().setBigContentTitle("Welldone"))
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(1251, notificationBuilder.build())
        }
    }


    companion object {
        private val TAG = "MyFirebaseMsgService"
    }
}