package com.app.letstalk.utils

import com.app.letstalk.adapters.ProductsResponse
import com.app.letstalk.fragments.SignupFragment
import com.app.letstalk.models.*
import retrofit2.Call
import retrofit2.http.*


interface ApiService {
   // @get:GET("random_images.php")
    //val allPhotos: Call<Model>


//    @GET("search.php")
//    fun getSearchResult(@Query("search") search: String) : Call<Image>


    @POST("api/api_login")
    @FormUrlEncoded
    fun onLogin(
                 @Field("email") email:String,
                 @Field("password") password:String,
                 @Field("device_type") deviceType:String,
                 @Field("device_token") deviceToken:String): Call<LoginResponse>



    @POST("api/api_signup")
    @FormUrlEncoded
    fun onSignup(@Field("first_name") fname:String,
                 @Field("last_name") lname:String,
                 @Field("email") email:String,
                 @Field("password") password:String,
                 @Field("device_type") deviceType:String,
                 @Field("device_token") deviceToken:String): Call<SignUpResponse>


    @POST("api/api_user_profile")
    @FormUrlEncoded
    fun getProfile(@Field("user_id") id:String,
                 @Field("access_token")token :String
                    ): Call<LoginResponse>


    @POST("api/api_forgot_password")
    @FormUrlEncoded
    fun forgotPassword(@Field("email") email:String): Call<ErrorWithMessage>


    @POST("api/api_verify_otp")
    @FormUrlEncoded
    fun otpVerify(@Field("email") email:String,
                   @Field("otp") otp:String
    ): Call<ErrorWithMessage>



    @POST("api/api_reset_password")
    @FormUrlEncoded
    fun resetPassword(@Field("email") email:String,
                  @Field("new_password") npass:String,
                  @Field("confirm_password") cpass:String
    ): Call<ErrorWithMessage>


    @POST("api/api_update_profile_image")
    @FormUrlEncoded
    fun updateProfileImage(@Field("user_id") id :String,
                      @Field("profile_image") image :String,
                      @Field("access_token") token :String
    ): Call<LoginResponse>


    @POST("api/api_update_profile")
    @FormUrlEncoded
    fun updateProfile(@Field("user_id") id :String,
                             @Field("first_name") fname :String,
                            @Field("last_name") lname :String,
                             @Field("mobile") mobile :String,
                             @Field("access_token") token :String
    ): Call<LoginResponse>


    @POST("api/api_update_password")
    @FormUrlEncoded
    fun updatePassword(@Field("user_id") id :String,
                      @Field("old_password") oldpassword :String,
                      @Field("new_password") newpassword :String,
                       @Field("confirm_password") cnfpassword :String,
                      @Field("access_token") token :String
    ): Call<ErrorWithMessage>

    @POST("api/api_add_address")
    @FormUrlEncoded
    fun addAddress(@Field("user_id") id :String,
                       @Field("name") name :String,
                       @Field("mobile") mobile :String,
                       @Field("address_line") addrone :String,
                       @Field("address_line2") addrtwo :String, //optionsal
                       @Field("city") city :String,
                       @Field("state") state:String,
                       @Field("country") country :String,
                       @Field("zip_code") zipcode :String,
                       @Field("access_token") token :String
    ): Call<ErrorWithMessage>

    @POST("api/api_delete_address")
    @FormUrlEncoded
    fun deleteAddress(@Field("user_id") id :String,
                       @Field("address_id") addrid :String,
                       @Field("access_token") token :String
    ): Call<ErrorWithMessage>



    @POST("api/api_get_address")
    @FormUrlEncoded
    fun getAddress(@Field("user_id") id :String,
                      @Field("access_token") token :String
    ): Call<AddressResponse>

    @POST("api/api_home")
    @FormUrlEncoded
    fun getHome(
        @Field("user_id") id :String,
        @Field("access_token") token :String
    ): Call<HomeResponse>

    @POST("api/api_product_sub_category")
    @FormUrlEncoded
    fun getCategoryData(@Field("user_id") id :String,
                      @Field("category_id") catid :String,
                      @Field("access_token") token :String
    ): Call<CategoryResponse>

    @POST("api/api_product_child_category")
    @FormUrlEncoded
    fun getCategoryChildData(@Field("user_id") id :String,
                        @Field("category_id") catid :String,
                        @Field("access_token") token :String
    ): Call<CategoryResponse>


    //@Headers("bearer_token: 9900a9720d31dfd5fdb4352700c")
    @POST("api/api_products_in_category")
    @FormUrlEncoded
    fun getProductInCategory(@Field("user_id") id :String,
                        @Field("category_id") catid :String,
                        @Field("access_token") token :String
    ): Call<ProductsResponse>

    @POST("api/api_products_details")
    @FormUrlEncoded
    fun getProductInfo(@Field("user_id") id :String,
                             @Field("product_id") pid :String,
                             @Field("access_token") token :String
    ): Call<ProductInfoResponse>

    @POST("api/api_get_filter")
    @FormUrlEncoded
    fun getFilter(@Field("user_id") id :String,
                       @Field("category_id") cid :String,
                       @Field("access_token") token :String
    ): Call<GetFilterResponse>

    @POST("api/api_add_to_cart")
    @FormUrlEncoded
    fun addTocart(@Field("user_id") id :String,
                  @Field("access_token") token :String,
                  @Field("product_id") pid :String,
                  @Field("color_id") cid :String,
                  @Field("size_id") sid :String,
                  @Field("qty") qty :String
    ): Call<ErrorWithMessage>


    @POST("api/api_cart_items")
    @FormUrlEncoded
    fun getCartItems(@Field("user_id") id :String,
                   @Field("access_token") token :String
    ): Call<CartResponse>


    @POST("api/api_remove_cart")
    @FormUrlEncoded
    fun removeCartItem(@Field("user_id") id :String,
                      @Field("cart_id") cartid :String,
                      @Field("access_token") token :String
    ): Call<ErrorWithMessage>




    @POST("api/api_create_order")
    @FormUrlEncoded
    fun createOrder(@Field("user_id") id :String,
                       @Field("access_token") token :String,
                    @Field("address_id") cartid :String,
                    @Field("txn_id") txnid :String,
                    @Field("amount") amount :String,
                    @Field("status") status :String

    ): Call<ErrorWithMessage>

    @POST("api/api_orders")
    @FormUrlEncoded
    fun getOrders(@Field("user_id") id :String,
                     @Field("access_token") token :String
    ): Call<MyOrdersResponse>


    @POST("api/api_contact_admin")
    @FormUrlEncoded
    fun onContact(
        @Field("title") title:String,
        @Field("message") message:String,
        @Field("user_id") user_id :String,
        @Field("access_token") access_token :String): Call<ErrorWithMessage>


    @POST("api/api_search_product")
    @FormUrlEncoded
    fun onSearchProducts(
        @Field("title") title:String,
        @Field("user_id") user_id :String,
        @Field("access_token") access_token :String): Call<SearchProducts>


    @POST("api/api_send_review")
    @FormUrlEncoded
    fun onReview(
        @Field("content") content:String,
        @Field("product_id") product_id:String,
        @Field("rating") rating:String,
        @Field("user_id") user_id :String,
        @Field("access_token") access_token :String): Call<ReviewResponse>


    @POST("api/api_filter_proudcts")
    @FormUrlEncoded
    fun applyFilter(

        @Field("user_id") user_id :String,
        @Field("access_token") access_token :String,
        @Field("category_id") catid :String,
        @Field("colors") colors :String,
        @Field("sizes") sizes :String,
        @Field("brands") brands :String,
        @Field("tags") tags :String,
        @Field("prices") prices :String)
            : Call<SearchProducts>



    @POST("api/api_notifications")
    @FormUrlEncoded
    fun getNotifications(@Field("user_id") id :String,
                       @Field("access_token") token :String
    ): Call<NotificationResponse>















}