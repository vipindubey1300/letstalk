package com.app.letstalk.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.text.HtmlCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.*
import com.app.letstalk.models.*
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.FieldPosition

class ProductInfoFragment : Fragment() , ColorSelect,SizeSelect{


    private var colorId = 0
    private var posColor = -1

    private var sizeId = 0
    private var posSize = -1

    private var quantity = 1


    private var sizesList : ArrayList<Product_sizes>? = null
    private var colorList : ArrayList<Product_colors>? = null

    override fun onSizeSelect(sizeSelected: Int, position: Int): Int {
        //case 1 fresh click
        if(posSize == -1){
            sizeId = sizeSelected
            posSize= position
            return 0
        }
        //select same color
        else if(colorId == sizeSelected && posSize == position){
            sizeId= -1
            colorId = 0
            return 1
        }
        //select diff color
        else{
            sizeId = sizeSelected
            posSize = position
            return 2
        }
    }

    override fun onColorSelect(colorSelected: Int,position: Int) : Int {
        Log.d("IDCOLOR-------",colorSelected.toString())

        //case 1 fresh click
        if(posColor == -1){
            colorId = colorSelected
            posColor = position
            return 0
        }
        //select same color
        else if(colorId == colorSelected && posColor == position){
            posColor = -1
            colorId = 0
            return 1
        }
        //select diff color
        else{
            colorId = colorSelected
            posColor = position
            return 2
        }
    }

    private lateinit var rootView : View


    private lateinit var expandableListView: ExpandableListView
//    internal var adapter: ExpandableListAdapter? = null
//    internal var titleList: List<String> ? = null
    private lateinit var adapter: ExpandableListAdapter
    private lateinit var titleList: List<String>

    private lateinit var call : Call<ProductInfoResponse>
     lateinit var callCart : Call<ErrorWithMessage>
    private lateinit var addCartBtn : Button


    private var buy_status = false



    private lateinit var avgRating : RatingBar
    private lateinit var avgReview : TextView







    private lateinit var plusButton : ImageView
    private lateinit var minusButton : ImageView
    private lateinit var quantityText : TextView

    private lateinit var ratingBar: RatingBar
    private lateinit var commentSection : LinearLayout

    private lateinit var commentEditText: EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_product_info, container, false)

        scrollView()

        addCartBtn = rootView.findViewById(R.id.addtocart_btn)
        commentSection = rootView.findViewById(R.id.commentSection)

        //LTRB...The order of params corresponding to the drawable location is: left, top, right, bottom
        //editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.drawableRight, 0)
        val product_id : Int = arguments!!.getInt("product_id")

        expandableListView = rootView.findViewById(R.id.expandableListView)


        avgRating = rootView.findViewById(R.id.avg_rating)
        avgReview = rootView.findViewById(R.id.avg_review)

        ratingBar = rootView.findViewById(R.id.productRatingBar)
        Log.d("RATING-----",ratingBar.rating.toString())

        plusButton  = rootView.findViewById(R.id.plus_button)
        minusButton  = rootView.findViewById(R.id.minus_button)
        quantityText = rootView.findViewById(R.id.quantity_text)

        commentEditText = rootView.findViewById(R.id.reviewEditText)


        if(buy_status == false){
            commentSection.visibility = View.GONE
        }
        else{
            commentSection.visibility = View.VISIBLE
        }


        commentEditText.setOnTouchListener(object: View.OnTouchListener {
            override fun onTouch(v:View, event: MotionEvent):Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.getAction() === MotionEvent.ACTION_UP)
                {
                    if (event.getRawX() >= (commentEditText.getRight() - commentEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                    {
                        if(commentEditText.text.toString().length > 0){
                            makeComment()
                        }

                        return true
                    }
                }
                return false
            }
        })


        quantityText.setText(quantity.toString())

        plusButton.setOnClickListener {
            var quant = quantityText.text.toString().toInt()
            quant = quant + 1
            quantityText.setText(quant.toString())

        }


        minusButton.setOnClickListener {
            var quant = quantityText.text.toString().toInt()
            if(quant > 1){
                quant = quant - 1
                quantityText.setText(quant.toString())
            }
        }

        fetch(product_id)


        addCartBtn.setOnClickListener {
            if(sizeId == 0 && sizesList!!.size > 0){
                Toast.makeText(activity!!,"Select Size",Toast.LENGTH_SHORT).show()
            }
            else if(colorId == 0 && colorList!!.size > 0){
                Toast.makeText(activity!!,"Select Color",Toast.LENGTH_SHORT).show()
            }
            else{
                addcart(product_id)
            }
        }


        return rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if(call != null && call.isExecuted()) {
            call.cancel();
        }
        //check if lateinit is initialized
        if(::callCart.isInitialized) {
            if(callCart != null && callCart.isExecuted()  ){
                callCart.cancel();
            }

        }
    }


    fun makeComment(){

        var auth_token : String? = null
        var id  : Int? = null

        //api call

        // Log.d("PRODUCT----",productId.toString())

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        // your action here
        val product_id : Int = arguments!!.getInt("product_id")

        var Commenttext = commentEditText.text.toString()
        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.onReview(Commenttext.toString(),
            product_id.toString(),
            ratingBar.rating.toString(),id!!.toString(),auth_token!!.toString()
        )


        //add prgress bar here
        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE

        Log.d("RATING-----",ratingBar.rating.toString())

        call.enqueue(object : Callback<ReviewResponse> {
            override fun onResponse(call: Call<ReviewResponse>, response: Response<ReviewResponse>) {


                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //progressDoalog.dismiss()
//                generateDataList(response.body())
//                Toast.makeText(this@MainActivity, response.body()!![0].title, Toast.LENGTH_SHORT).show()
                var responseJson = response.body()
                if(responseJson!!.error == false ){

                    commentEditText.setText("")

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()


                    //comment adapter
                    val commentRecycler = rootView.findViewById<RecyclerView>(R.id.comment_recycler)
                    commentRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL ,false)
                    commentRecycler.adapter = CommentAdapter(activity!!,ArrayList(responseJson!!.result))


                }
                else{
                    Toast.makeText(activity!!, "Error !", Toast.LENGTH_SHORT).show()

                }
                Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
                //List<RetroPhoto> photoList
            }

            override fun onFailure(call: Call<ReviewResponse>, t: Throwable) {


                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                // progressDoalog.dismiss()
                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                Log.d("SADFSDFASDFSDFSDFSD",t.message)
            }
        })
    }

    fun fetch(productId : Int){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        Log.d("PRODUCT----",productId.toString())

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
         call = service.getProductInfo(id!!.toString(),productId.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<ProductInfoResponse> {

            override fun onResponse(call: Call<ProductInfoResponse>, response: Response<ProductInfoResponse>) {


                var responseJson = response.body()

                Log.e("TAG", " " + Gson().toJson(response.body()))

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE


                if(!responseJson!!.error){



                    //review

                    var rating =
                        if (responseJson!!.result.avg_rating.length > 3)
                            (responseJson!!.result.avg_rating.toString().substring(0,3))
                        else(responseJson!!.result.avg_rating)


                    avgReview.setText(rating)
                    avgRating.rating = responseJson!!.result.avg_rating.toFloat()

                    buy_status = responseJson.result.buy_status

                    if(buy_status){
                        commentSection.visibility = View.VISIBLE
                    }

                    //expandalble adapter

                    val listData = HashMap<String, List<String>>()


                    val keyFeatures = ArrayList<String>()
                    val environment = ArrayList<String>()
                    val goodToKnow = ArrayList<String>()
                    val assembely = ArrayList<String>()



                    keyFeatures.add(responseJson!!.result.key_feature_feature)
                    environment.add(responseJson!!.result.environment)
                    goodToKnow.add(responseJson!!.result.good_to_know)
                    assembely.add(responseJson!!.result.assembly_info)



                    listData["Key Features"] = keyFeatures
                    listData["Environment  & materials"] = environment
                    listData["Good To know"] = goodToKnow
                    listData["Assembly Instructions/ Documents"] = assembely




                    if (expandableListView != null) {

                        //

                        titleList = ArrayList(listData.keys)

                        adapter = ProductExpandableAdapter(activity!!, titleList as ArrayList<String>, listData)
                        expandableListView!!.setAdapter(adapter)

                        Log.d("EXApndable========",expandableListView!!.toString())

                        //cheat code so that expandable is full shown
                        setListViewHeight(expandableListView!!, 0)


                        // expandableListView!!.setOnGroupExpandListener { groupPosition -> Toast.makeText(activity!!, (titleList as ArrayList<String>)[groupPosition] + " List Expanded.", Toast.LENGTH_SHORT).show() }

                        // expandableListView!!.setOnGroupCollapseListener { groupPosition -> Toast.makeText(activity!!, (titleList as ArrayList<String>)[groupPosition] + " List Collapsed.", Toast.LENGTH_SHORT).show() }

                        expandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                            // Toast.makeText(activity!!, "Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + listData[(titleList as ArrayList<String>)[groupPosition]]!!.get(childPosition), Toast.LENGTH_SHORT).show()
                            false
                        }

                        expandableListView!!.setOnGroupClickListener(object:ExpandableListView.OnGroupClickListener {
                            override fun onGroupClick(parent:ExpandableListView, v:View,
                                                      groupPosition:Int, id:Long):Boolean {
                                setListViewHeight(parent, groupPosition)
                                return false
                            }
                        })
                    }


                    sizesList = ArrayList(responseJson!!.result.product_sizes)
                    colorList = ArrayList(responseJson!!.result.product_colors)


                    //size adapter
                    val sizeRecycler = rootView.findViewById<RecyclerView>(R.id.product_size_recycler)
                    sizeRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL ,false)
                    sizeRecycler.adapter = SizeAdapter(activity!!,ArrayList(responseJson!!.result.product_sizes),this@ProductInfoFragment)

                    //color adapter
                    val colorRecycler = rootView.findViewById<RecyclerView>(R.id.product_color_recycler)
                    colorRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL ,false)
                    colorRecycler.adapter = ColorAdapter(activity!!,ArrayList(responseJson!!.result.product_colors),this@ProductInfoFragment)



                    //name of product

                    val name = rootView.findViewById<TextView>(R.id.product_name)
                    val price = rootView.findViewById<TextView>(R.id.product_price)
                    val description= rootView.findViewById<TextView>(R.id.product_description)
                    name.text = responseJson!!.result.name
                    price.text = "$ " + responseJson!!.result.price.toString()
                    description.text = HtmlCompat.fromHtml(responseJson!!.result.description, HtmlCompat.FROM_HTML_MODE_COMPACT)


                    //gallery

                    var viewPager = rootView.findViewById<ViewPager>(R.id.product_viewPager)
                    val tabLayout = rootView.findViewById(R.id.product_tabDots) as TabLayout
                    viewPager.adapter = ProductViewPager(activity!!,ArrayList(responseJson!!.result.gallery))
                    tabLayout.setupWithViewPager(viewPager)


                    //comment

                    //comment adapter
                   if(responseJson!!.result.reviews.size > 0){
                       val commentRecycler = rootView.findViewById<RecyclerView>(R.id.comment_recycler)
                       commentRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL ,false)
                       commentRecycler.adapter = CommentAdapter(activity!!,ArrayList(responseJson!!.result.reviews))

                   }
                }



            }

            override fun onFailure(call: Call<ProductInfoResponse>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                if (call.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }


    fun scrollView(){
        var scroll_view = rootView.findViewById<NestedScrollView>(R.id.scrollView)

        scroll_view.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener {
                    _, scrollX, scrollY, _, oldScrollY ->

                if(scrollY > oldScrollY){
                    //Toast.makeText(activity!!,"Up",Toast.LENGTH_SHORT).show()
                   if(buy_status){
                       commentSection.visibility = View.GONE
                   }
                }
                else{
                    //Toast.makeText(activity!!,"Down",Toast.LENGTH_SHORT).show()
                    if(buy_status){
                        commentSection.visibility = View.VISIBLE
                    }

                }
            })


     //   if (scroll_view != null)
//        {
//            scroll_view.setOnScrollChangeListener(object:ScrollView.OnScrollChangeListener {
//                override fun onScrollChange(v:NestedScrollView, scrollX:Int, scrollY:Int, oldScrollX:Int, oldScrollY:Int) {
//                    if(scrollY > oldScrollY )
//                    {
//                        bottom_bar.visibility =View.GONE
//                        line.visibility = View.GONE
//                        img_fab.visibility = View.GONE
//                    }
//                    else{
//                        bottom_bar.visibility = View.VISIBLE
//                        line.visibility = View.VISIBLE
//                        img_fab.visibility = View.VISIBLE
//                    }
//                    if (scrollY < oldScrollY)
//                    {
//                        // Log.i(TAG, "Scroll UP")
//                        //     Toast.makeText(activity!!,"Scroll up",Toast.LENGTH_SHORT).show()
//                    }
//                    if (scrollY == 0)
//                    {
//                        // Log.i(TAG, "TOP SCROLL")
//                        // Toast.makeText(activity!!,"TOP SCROLL",Toast.LENGTH_SHORT).show()
//                    }
//                    if (scrollY == (v.getMeasuredHeight() - v.getChildAt(0).getMeasuredHeight()))
//                    {
//                        // Log.i(TAG, "BOTTOM SCROLL")
//                        //  Toast.makeText(activity!!,"BOTTOM SCROLL",Toast.LENGTH_SHORT).show()
//                        //not wrking
//                    }
//                    val view = scroll_view.getChildAt(scroll_view.getChildCount() - 1) as View
//                    val diff = (view.getBottom() - (scroll_view.getHeight() + scroll_view.getScrollY()))
//                    if (diff == 0)
//                    {
//                        //if else is good for many layout visibility
////                        if(nsv_counter == 0){
////                            //means pehle baar battom
////                            Toast.makeText(activity!!,"Loading firsrt !!",Toast.LENGTH_SHORT).show()
////                            Handler().postDelayed({
////                                //doSomethingHere()
////                                val layout = rootView.findViewById<LinearLayout>(R.id.new_layout)
////                                layout.visibility=View.VISIBLE
////                            }, 3000)
////
////                            nsv_counter++
////                        }
////                        else if(nsv_counter == 1){
////                            //dusri baar yaar
////                            Toast.makeText(activity!!,"Loading two !!",Toast.LENGTH_SHORT).show()
////                            Handler().postDelayed({
////                                //doSomethingHere()
////                                val layout = rootView.findViewById<LinearLayout>(R.id.new_layout_two)
////                                layout.visibility=View.VISIBLE
////                            }, 3000)
////
////                            nsv_counter++
////                        }
////                        else{
////                            Toast.makeText(activity!!,"Thats all !!",Toast.LENGTH_SHORT).show()
////                        }
//                        // notify that we have reached the bottom
//
//                        Toast.makeText(activity!!,"Thats all !!",Toast.LENGTH_SHORT).show()
////                        val layout = rootView.findViewById<LinearLayout>(R.id.new_layout)
////                        layout.visibility=View.VISIBLE
//                    }
//                }
//            })
//        }

//      scroll_view.viewTreeObserver.addOnScrollChangedListener(
//              object:ViewTreeObserver.OnScrollChangedListener {
//                  override fun onScrollChanged() {
//                      //val x = scroll_view.getScrollX()
//                      val y = scroll_view.getScrollY()
//                      if(y < 0 )
//                      {
//                          bottom_bar.visibility == View.GONE
//                          line.visibility == View.GONE
//                      }
//                      else{
//                          bottom_bar.visibility == View.VISIBLE
//                          line.visibility == View.VISIBLE
//                      }
//                  }
//              }
//      )
//
//

    }

    fun addcart(productId : Int){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        callCart = service.addTocart(id!!.toString(),auth_token!!,productId.toString()
            ,colorId.toString(),sizeId.toString(),quantityText.text.toString())

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        callCart.enqueue(object : Callback<ErrorWithMessage> {

            override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE

                Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                if(!responseJson!!.error){
                    var cart_text= (activity as HomeActivity).findViewById<TextView>(R.id.cart_text)
                    var quat = cart_text.text.toString().toInt()
                    quat = quat + 1
                    cart_text.setText(quat.toString())
                }





            }

            override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                if (callCart.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }

    private fun setListViewHeight(listView:ExpandableListView,
                                  group:Int) {
        val listAdapter = listView.getExpandableListAdapter() as ExpandableListAdapter
        var totalHeight = 0
        val desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
            View.MeasureSpec.EXACTLY)
        for (i in 0 until listAdapter.getGroupCount())
        {
            val groupItem = listAdapter.getGroupView(i, false, null, listView)
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
            totalHeight += groupItem.getMeasuredHeight()
            if ((((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))))
            {
                for (j in 0 until listAdapter.getChildrenCount(i))
                {
                    val listItem = listAdapter.getChildView(i, j, false, null,
                        listView)
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                    totalHeight += listItem.getMeasuredHeight()
                }
            }
        }
        val params = listView.getLayoutParams()
        var height = (totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1)))
        if (height < 10)
            height = 200
        params.height = height
        listView.setLayoutParams(params)
        listView.requestLayout()
    }


}
