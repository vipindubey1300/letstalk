package com.app.letstalk.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.CartAdapter
import com.app.letstalk.adapters.MyOrdersAdapter
import com.app.letstalk.adapters.ProductAdapter
import com.app.letstalk.models.MyOrdersResponse
import com.app.letstalk.models.Products
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.android.material.navigation.NavigationView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyOrdersFragment : Fragment() {

    private lateinit var rootView : View
    private lateinit var call : Call<MyOrdersResponse>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_my_orders, container, false)

        (activity as HomeActivity).findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.my_orders);


        fetch()

        return rootView
    }


    override fun onDestroyView() {
        super.onDestroyView()
        if(call != null && call.isExecuted()) {
            call.cancel();
        }

    }

    fun fetch(){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        call = service.getOrders(id!!.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<MyOrdersResponse> {

            override fun onResponse(call: Call<MyOrdersResponse>, response: Response<MyOrdersResponse>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE


                if(!responseJson!!.error){



                    //size adapter
                    val my_ordersRecycler = rootView.findViewById<RecyclerView>(R.id.my_orders_recycler)
                    my_ordersRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
                    val adapter = MyOrdersAdapter(activity!!, ArrayList(responseJson!!.result!!))
                    my_ordersRecycler.adapter = adapter

                }

                //Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

            }

            override fun onFailure(call: Call<MyOrdersResponse>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                if (call.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }



}
