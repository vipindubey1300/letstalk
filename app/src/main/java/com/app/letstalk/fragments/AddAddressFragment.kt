package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response





class AddAddressFragment : Fragment() {

    private lateinit var rootView : View

    private lateinit var nameEdit : EditText
    private lateinit var phoneEdit : EditText
    private lateinit var addroneEdit : EditText
    private lateinit var addrtwoEdit : EditText
    private lateinit var cityEdit : EditText
    private lateinit var stateEdit : EditText
    private lateinit var zipcodeEdit : EditText
    private lateinit var countryEdit : EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_add_address, container, false)

        nameEdit = rootView.findViewById(R.id.name)
        phoneEdit = rootView.findViewById(R.id.phone_no)
        addroneEdit = rootView.findViewById(R.id.address_one)
        addrtwoEdit = rootView.findViewById(R.id.address_two)
        cityEdit = rootView.findViewById(R.id.city)
        stateEdit = rootView.findViewById(R.id.state)
        zipcodeEdit= rootView.findViewById(R.id.postal)
        countryEdit = rootView.findViewById(R.id.country)

        val addAddress = rootView.findViewById<Button>(R.id.add_address_btn)


        addAddress.setOnClickListener {
            addAddress(nameEdit.text.toString()
            ,phoneEdit.text.toString()
            ,addroneEdit.text.toString()
                ,addrtwoEdit.text.toString()
                ,cityEdit.text.toString()
                ,stateEdit.text.toString()
                ,zipcodeEdit.text.toString()
                ,countryEdit.text.toString())
        }


        return rootView
    }

    fun isValid(name :String , phone:String ,addrOne : String , city: String,state:String,postal:String,country : String) : Boolean{
        var valid = false

//        val re = Regex("[^A-Za-z0-9 ]")
//        answer = re.replace(answer, "")

        var phoneRegex = "\\d+"

        //val thePattern = "[a-zA-Z0-9]*" //no special char
        val thePattern = "[a-zA-Z]*"


        if(name.trim().length > 0 && phone.trim().length > 0
            && phone.matches((phoneRegex).toRegex())
            &&  !(phone.trim().length < 10 || phone.trim().length >= 17)
            && addrOne.trim().length > 0
            && city.trim().length > 0
            && state.trim().length > 0
            && postal.trim().length > 0 && country.trim().length > 0
            && country.matches((thePattern).toRegex())){

            return true
        }
        else if(name.trim().length == 0){

            nameEdit.requestFocus()
            nameEdit.error = "Enter Name"
            return false
        }
        else if(phone.trim().length == 0){
            phoneEdit.requestFocus()
            phoneEdit.error = "Enter Mobile no."
            return false
        }
        else if(phone.matches((phoneRegex).toRegex()) == false){
            phoneEdit.requestFocus()
            phoneEdit.error = "Enter valid Mobile no."
            return false
        }
        else if(phone.trim().length < 10 || phone.trim().length >= 17){
            phoneEdit.requestFocus()
            phoneEdit.error ="Mobile should be 10-16 digits long"
            return false
        }
        else if(addrOne.trim().length == 0){
            addroneEdit.requestFocus()
            addroneEdit.error = "Enter Address line 1"
            return false
        }
        else if(city.trim().length == 0){
            cityEdit.requestFocus()
            cityEdit.error = "Enter City"
            return false
        }
        else if(city.matches((thePattern).toRegex()) == false){
            cityEdit.requestFocus()
            cityEdit.error = "Enter valid City"
            return false
        }
        else if(state.trim().length == 0){
            stateEdit.requestFocus()
            stateEdit.error = "Enter State"
            return false
        }

        else if(state.matches((thePattern).toRegex()) == false){
            stateEdit.requestFocus()
            stateEdit.error = "Enter Valid State"
            return false
        }
        else if(postal.trim().length == 0){
            zipcodeEdit.requestFocus()
            zipcodeEdit.error = "Enter Postal Code"
            return false
        }
        else if(country.trim().length == 0){
            countryEdit.requestFocus()
            countryEdit.error = "Enter Country"
            return false
        }
        else if(country.matches((thePattern).toRegex()) == false){
            countryEdit.requestFocus()
            countryEdit.error = "Enter Valid Country"
            return false
        }

        return valid
    }

    fun addAddress(name :String , phone:String ,addrOne : String ,addrTwo: String, city: String,state:String,postal:String,country : String){

        Log.d("VALID>>>>>>>",isValid(name,phone,addrOne,city,state,postal,country).toString())
        if(isValid(name,phone,addrOne,city,state,postal,country)){

            var auth_token : String? = null
            var id  : Int? = null

            //api call

            val PREFS_FILENAME = "user_details"
            val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

            if(prefs.contains("id")) {
                //means logged in

                auth_token = prefs.getString("auth_token","")
                id = prefs.getInt("id",0)


            }

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.addAddress(id!!.toString(),name,phone,addrOne,addrTwo,city,state,country,postal,auth_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE


            call.enqueue(object : Callback<ErrorWithMessage> {


                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                    var responseJson = response.body()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){

                      //  activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame,AddressFragment()).addToBackStack(null).commit()
                        activity!!. supportFragmentManager.popBackStack()

//                        val mainIntent = Intent(activity!! as HomeActivity, HomeActivity::class.java)
//                        (activity!! as HomeActivity).startActivity(mainIntent)
//                        (activity!! as HomeActivity).finish()

                    }

                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                    Log.d("FAILUREEEEE",t.message)

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    // progressDoalog.dismiss()
                    //pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })


        }
    }


}
