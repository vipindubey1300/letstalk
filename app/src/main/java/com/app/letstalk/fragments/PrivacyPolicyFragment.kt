package com.app.letstalk.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.utils.Constants
import com.google.android.material.navigation.NavigationView


class PrivacyPolicyFragment : Fragment() {

    private lateinit var rootView : View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {



        rootView = inflater.inflate(R.layout.fragment_privacy_policy, container, false)

        val view =  (activity!!).findViewById<NavigationView>(R.id.nav_view)

        if(view == null){

        }
        else{
            (activity as HomeActivity).findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.privacy_policy);

        }



        val privacyWebview : WebView = rootView.findViewById(R.id.privacy_web_view)
        privacyWebview.loadUrl(Constants.link + "api/api_privacy_policy")

        return rootView
    }



}
