package com.app.letstalk.fragments


import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.Constants
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException


class EditProfileFragment : Fragment() {

    private lateinit var rootView : View
    private lateinit var profileImage : ImageView

    private lateinit var bitmapImage : Bitmap

    private lateinit var firstName : String
    private lateinit var lastName : String
    private lateinit var emailUser : String
    private lateinit var phoneUser : String
    private lateinit var imageUser : String


    private lateinit var fnameEdit : EditText
    private lateinit var lnameEdit : EditText
    private lateinit var phoneEdit : EditText
    private lateinit var emailEdit : EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_edit_profile, container, false)

        fnameEdit = rootView.findViewById(R.id.first_name)
        lnameEdit = rootView.findViewById(R.id.last_name)
        phoneEdit = rootView.findViewById(R.id.phone)
        emailEdit = rootView.findViewById(R.id.email_address)

        //setting values

        firstName  = arguments!!.getString("first_name")
        lastName  = arguments!!.getString("last_name")
        emailUser = arguments!!.getString("email")
        phoneUser = arguments!!.getString("mobile")
        imageUser = arguments!!.getString("user_image")

        val thePattern ="\\d+"

        Log.d("PHOneeeee0000000000",phoneUser.matches((thePattern).toRegex()).toString())


        profileImage = rootView.findViewById(R.id.profile_image)


      //  Picasso.with(context).load(Constants.link+imageUser).into(profileImage)

        Picasso.with(context).load(Constants.link+imageUser)
            .resize(150,150)
            .into(profileImage, object: com.squareup.picasso.Callback {
                override fun onSuccess() {
                    val imageBitmap = (profileImage.getDrawable() as BitmapDrawable).getBitmap()
                    val imageDrawable = RoundedBitmapDrawableFactory.create(context!!.getResources(), imageBitmap)
                    imageDrawable.setCircular(true)
                    imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f)
                    profileImage.setImageDrawable(imageDrawable)
                }
                override fun onError() {
                    //viewHolder.color_image.setImageResource(R.drawable.default_image)
                }
            })


        

        fnameEdit.setText(firstName)
        lnameEdit.setText(lastName)


        if(phoneUser.matches((thePattern).toRegex()) == false){
            //means string
            Log.d("ifffff","ifffff")
        }
        else{
            Log.d("Elseee","elseeee")
            phoneEdit.setText(phoneUser)
        }



        emailEdit.setText(emailUser)


        val changeImage = rootView.findViewById<ImageView>(R.id.change_image)
        changeImage.setOnClickListener {

            val WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val CAMERA = ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA)



            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                val permissions = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.CAMERA)
//                ActivityCompat.requestPermissions(activity!!, permissions, 0)
                if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED && CAMERA != PackageManager.PERMISSION_GRANTED)
                {

                        val permissions = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.CAMERA)
                       // ActivityCompat.requestPermissions(activity!!, permissions, 0)
                    //above for actiivty


                    requestPermissions( permissions, 0)



                }
                else{
                    showDialog()
                }




            }
//            showDialog(
        }


        val submitBtn = rootView.findViewById<Button>(R.id.save_button)
        submitBtn.setOnClickListener {
            editProfile(fnameEdit.text.toString(),lnameEdit.text.toString(),phoneEdit.text.toString())

        }



        return rootView
    }

    fun isValid(fname: String, lname: String, mobile: String) : Boolean{
        var valid = false
        var phoneRegex = "\\d+"

        Log.d("PHONE----------", (mobile.trim().length < 10 || mobile.trim().length >= 17).toString())

        if(fname.trim().length > 0 && lname.trim().length > 0 &&
            !(mobile.trim().length < 10 || mobile.trim().length >= 17) &&
            mobile.trim().length > 0  && mobile.matches((phoneRegex).toRegex())){
            return true
        }
        else if(fname.trim().length == 0){
            fnameEdit.requestFocus()
            fnameEdit.error ="Enter first name"
            return false
        }
        else if(lname.trim().length == 0){
            lnameEdit.requestFocus()
            lnameEdit.error ="Enter last name"
            return false
        }
        else if(mobile.trim().length == 0){
            phoneEdit.requestFocus()
            phoneEdit.error ="Enter mobile no."
            return false
        }

        else if(mobile.matches((phoneRegex).toRegex()) == false){
            phoneEdit.requestFocus()
            phoneEdit.error = "Enter valid Mobile no."
            return false
        }
        else if(mobile.trim().length < 10 || mobile.trim().length >= 17){
            phoneEdit.requestFocus()
            phoneEdit.error ="Mobile no. should be 10-16 digits long"
            return false
        }


        return  valid
    }

    fun editProfile(fname: String, lname: String, mobile: String){
        if(isValid(fname,lname,mobile)){


            var auth_token : String? = null
            var id  : Int? = null

            //api call

            val PREFS_FILENAME = "user_details"
            val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

            if(prefs.contains("id")) {
                //means logged in

                auth_token = prefs.getString("auth_token","")
                id = prefs.getInt("id",0)


            }

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.updateProfile(id!!.toString(),fname,lname,mobile,auth_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE


            call.enqueue(object : Callback<LoginResponse> {


                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {


                    var responseJson = response.body()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){




                        val PREFS_FILENAME = "user_details"
                        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                        val editor = prefs.edit()

                        editor.putString("name",responseJson!!.result.name)

                        editor.apply()

                        val mainIntent = Intent(activity!! as HomeActivity, HomeActivity::class.java)
                        (activity!! as HomeActivity).startActivity(mainIntent)
                        (activity!! as HomeActivity).finish()

                    }

                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {

                    Log.d("FAILUREEEEE",t.message)

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    // progressDoalog.dismiss()
                    //pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })


        }

    }


    fun showDialog(){
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // dialog.setCancelable(false);
        dialog.setContentView(R.layout.choose_image_dialoge);


        dialog.show()


        val cameraBtn = dialog.findViewById<Button>(R.id.camera_button)
        val galleryBtn = dialog.findViewById<Button>(R.id.gallery_button)


        cameraBtn.setOnClickListener {
            dialog.dismiss()
            var takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePicture,0)
        }

        galleryBtn.setOnClickListener {
            dialog.dismiss()
            var pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            //var pickPhoto = Intent(Intent.ACTION_PICK)
            startActivityForResult(pickPhoto,1)
        }
    }

    fun onChangeImage(image : String){


        var auth_token : String? = null
        var id  : Int? = null

            //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

             auth_token = prefs.getString("auth_token","")
             id = prefs.getInt("id",0)


        }

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.updateProfileImage(id!!.toString(),image,auth_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE


            call.enqueue(object : Callback<LoginResponse> {


                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {


                    var responseJson = response.body()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){

                        profileImage.setImageBitmap(bitmapImage)
                        val newImage = responseJson.result.user_image


                        val PREFS_FILENAME = "user_details"
                        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                        val editor = prefs.edit()

                        editor.putString("image",newImage)

                        editor.apply()

                        val mainIntent = Intent(activity!! as HomeActivity, HomeActivity::class.java)
                        (activity!! as HomeActivity).startActivity(mainIntent)
                        (activity!! as HomeActivity).finish()

                    }

                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {

                    Log.d("FAILUREEEEE",t.message)

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    // progressDoalog.dismiss()
                    //pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
       // Toast.makeText(activity!!, "Callled", Toast.LENGTH_LONG).show()



        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start savinng image

           showDialog()

        } else {

            Toast.makeText(activity!!, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //0 means camera
        //1 means gallery

        Log.d("RESULT------------",resultCode.toString())


        when(requestCode) {
            0 -> {
                //uri to bitmap

                if(data != null){
                    bitmapImage = data!!.extras.get("data") as Bitmap
                    var encodedImage = encodeImageBitmap(bitmapImage )
                    //encoded images is in base 64
                    // print(encodedImage)

                    onChangeImage(encodedImage)
                }
            }
            1 ->{



            if(data != null){
                val imageUri = data?.getData()

                //convert to bitmap
                bitmapImage  = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, imageUri)

                //convert in base 64
                var encodedImage = encodeImageBitmap(bitmapImage)
                onChangeImage(encodedImage)
            }

            }

            else -> println("Invalid Day")
        }

    }


    private fun encodeImageUri(path:String):String {

        Log.d("PATH---------",path)
        val imagefile = File(path)
        var fis: FileInputStream? = null
        try
        {
            fis = FileInputStream(imagefile)
        }
        catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        val bm = BitmapFactory.decodeStream(fis)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        val encImage = Base64.encodeToString(b, Base64.DEFAULT)
        //Base64.de
        return encImage
    }


    private fun encodeImageBitmap(bm:Bitmap):String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
        return encoded
    }



}





/*
  if (VERSION.SDK_INT >= VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
 */
