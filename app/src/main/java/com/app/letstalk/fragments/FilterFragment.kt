package com.app.letstalk.fragments


import android.content.Context
import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.adapters.*
import com.app.letstalk.models.GetFilterResponse
import com.app.letstalk.models.SearchProducts
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbRangeSeekbar
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar





class FilterFragment(callback:ProductFragment) : Fragment() , BrandSelectFilter,TagSelectFilter,SizeSelectFilter,ColorSelectFilter{


    private lateinit var rootView : View
    private var brandListFilter = ArrayList<Int>()
    private var tagListFilter = ArrayList<Int>()
    private var colorListFilter = ArrayList<Int>()
    private var sizeListFilter = ArrayList<Int>()

    private var brandsPara = ""
    private var tagsPara = ""
    private var sizesPara = ""
    private var colorsPara = ""



    private var maxPrice = 0.0f
    private var minPrice = 0.0f

    var callback : ApplyFilter

    init {

        this.callback = callback
    }

    override fun onBrandSelectFilter(brandSelected: Int, position: Int): Boolean {
        val id = brandSelected
        if(brandListFilter.contains(brandSelected)){
            //means already
            //so deselect
            brandListFilter.remove(brandSelected)
            return false
        }
        else{
            brandListFilter.add(brandSelected)
            return true
        }

    }

    override fun onTagSelectFilter(tagSelected: Int): Boolean {
        if(tagListFilter.contains(tagSelected)){
            //means already
            //so deselect
            tagListFilter.remove(tagSelected)
            return false
        }
        else{
            tagListFilter.add(tagSelected)
            return true
        }
    }

    override fun onSizeSelectFilter(sizeSelected: Int): Boolean {
        if(sizeListFilter.contains(sizeSelected)){
            //means already
            //so deselect
            sizeListFilter.remove(sizeSelected)
            return false
        }
        else{
            sizeListFilter.add(sizeSelected)
            return true
        }
    }

    override fun onColorSelectFilter(colorSelected: Int): Boolean {
        if(colorListFilter.contains(colorSelected)){
            //means already
            //so deselect
            colorListFilter.remove(colorSelected)
            return false
        }
        else{
            colorListFilter.add(colorSelected)
            return true
        }
    }




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_filter, container, false)

        val closeFilter = rootView.findViewById<ImageView>(R.id.close_filter)
        closeFilter.setOnClickListener {
            fragmentManager!!.popBackStack()
            callback.onFilter()
        }

        val category_id : Int = arguments!!.getInt("category_id")

        Log.d("CAT_____",category_id.toString())


        var applyButton = rootView.findViewById<Button>(R.id.apply_btn)
        applyButton.setOnClickListener {
            for(i in 0..brandListFilter.size-1){
               // Log.d("BRANDS>>>>>>>>",brandListFilter.get(i).toString())

                brandsPara = brandsPara + brandListFilter.get(i).toString() + ","

            }
            for(i in 0..tagListFilter.size-1){

                tagsPara = tagsPara + tagListFilter.get(i).toString() + ","

            }
            for(i in 0..colorListFilter.size-1){

                colorsPara = colorsPara + colorListFilter.get(i).toString() + ","

            }

            for(i in 0..sizeListFilter.size-1){

                sizesPara = sizesPara + sizeListFilter.get(i).toString() + ","

            }

            Log.d("BRANDS--",brandsPara)
            Log.d("TAGS--",tagsPara)
            Log.d("SIZES--",sizesPara)
            Log.d("COLORS--",colorsPara)
            Log.d("MIN--",minPrice.toString())
            Log.d("MAX--",maxPrice.toString())


            applyFilters()
        }




        getFilters(category_id)

        return rootView
    }


    fun getFilters(categoryId : Int){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.getFilter(id!!.toString(),categoryId.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<GetFilterResponse> {


            override fun onResponse(call: Call<GetFilterResponse>, response: Response<GetFilterResponse>) {


                var responseJson = response.body()

                Log.e("TAG", " " + Gson().toJson(response.body()))

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE




                if(responseJson!!.error == false ){

                    val tags = responseJson!!.result.tags
                    val brands = responseJson!!.result.brands
                    val sizes = responseJson!!.result.sizes
                    val colors = responseJson!!.result.colors


                    //size adapter
                    val sizeRecycler = rootView.findViewById<RecyclerView>(R.id.sizes_recycler)
                    sizeRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL ,false)
                    sizeRecycler.adapter = SizeAdapFilter(activity!!,ArrayList(sizes),this@FilterFragment)

                    //color adapter
                    val colorRecycler = rootView.findViewById<RecyclerView>(R.id.colors_recycler)
                    colorRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL ,false)
                    colorRecycler.adapter = ColorAdapFilter(activity!!,ArrayList(colors),this@FilterFragment)

                    //brand adapter

                    val brandRecycler = rootView.findViewById<RecyclerView>(R.id.brands_recycler)
                    brandRecycler.layoutManager = GridLayoutManager(activity!!,3)
                    brandRecycler.adapter = BrandAdapFilter(activity!!,ArrayList(brands),this@FilterFragment)


                    for(i in 0..brands.size-1){
                        Log.d("FFF",brands.get(i).name)
                    }

                    //tag adapter

                    val tagRecycler = rootView.findViewById<RecyclerView>(R.id.tags_recycler)
                    tagRecycler.layoutManager = GridLayoutManager(activity!!,3)
                    tagRecycler.adapter = TagAdapFilter(activity!!,ArrayList(tags),this@FilterFragment)


//                    //seekbar
//                    val seekBar = RangeSeekBar<Float>(activity!!)
//                    seekBar.setRangeValues(responseJson!!.result.min_price.toFloat(), responseJson!!.result.max_price.toFloat())
//                    seekBar.setOnRangeSeekBarChangeListener(object:RangeSeekBar.OnRangeSeekBarChangeListener<Float> {
//                        override fun onRangeSeekBarValuesChanged(bar:RangeSeekBar<*>, minValue:Float, maxValue:Float) {
//                            //Now you have the minValue and maxValue of your RangeSeekbar
//
//                            Toast.makeText(activity!!, minValue.toString() + "-" + maxValue.toString(), Toast.LENGTH_LONG).show()
//                        }
//                    })
//
//                    // Get noticed while dragging
//                    seekBar.setNotifyWhileDragging(true);



                    val seekbar = rootView.findViewById<BubbleThumbRangeSeekbar>(R.id.rangeSeekbar)
                    //val seekbar = CrystalSeekbar(getActivity());


                    seekbar.setMinValue(responseJson!!.result.min_price.toFloat())
                    seekbar.setMaxValue(responseJson!!.result.max_price.toFloat())

                    seekbar.setOnRangeSeekbarChangeListener(object:
                        OnRangeSeekbarChangeListener{
                        override fun valueChanged(minValue:Number, maxValue:Number) {

                            Log.d("SSSSSS",minValue.toString())
                            Log.d("MMMMM",maxValue.toString())
                            minPrice = minValue.toFloat()
                            maxPrice = maxValue.toFloat()

                        }
                    })

//

                }

            }

            override fun onFailure(call: Call<GetFilterResponse>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun applyFilters(){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }
        val category_id : Int = arguments!!.getInt("category_id")

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.applyFilter(id!!.toString(),auth_token!!,category_id.toString(),
            colorsPara,sizesPara,brandsPara,tagsPara,(minPrice.toString() + "," + maxPrice.toString()))

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<SearchProducts> {


            override fun onResponse(call: Call<SearchProducts>, response: Response<SearchProducts>) {


                var responseJson = response.body()

                Log.e("TAG", " " + Gson().toJson(response.body()))

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE




                if(responseJson!!.error == false ){

                    if(responseJson!!.result.size > 0){
//


                        var frag: ProductFragment = ProductFragment()
                        var bundle: Bundle = Bundle()
                        bundle.putParcelable("myObject", responseJson)

                        frag.arguments = bundle


                        activity!!.supportFragmentManager.popBackStack()
                        activity!!.supportFragmentManager.
                            beginTransaction().
                            replace(R.id.home_frame,frag).addToBackStack(null).
                            commit()

                    }
                    else{
                        Toast.makeText(activity!!, "No Products Found", Toast.LENGTH_SHORT).show()
                    }
                }

            }

            override fun onFailure(call: Call<SearchProducts>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })
    }


}

interface ApplyFilter{
    fun onFilter()
}
