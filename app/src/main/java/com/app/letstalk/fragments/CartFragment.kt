package com.app.letstalk.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.CartAdapter
import com.app.letstalk.adapters.CartRemove
import com.app.letstalk.adapters.SizeAdapter
import com.app.letstalk.models.CartResponse
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.models.ProductInfoResponse
import com.app.letstalk.models.ResultCart
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CartFragment : Fragment() , CartRemove{



    private lateinit var rootView : View
    private lateinit var call : Call<CartResponse>

    private lateinit var adapter : CartAdapter
    private lateinit var cartList : ArrayList<ResultCart>

    private var subTotal =0.0
    private var shipping = 0.0
    private var tax  = 0.0

    private lateinit var subTotalText : TextView
    private lateinit var shippingText : TextView
    private lateinit var taxText : TextView
    private lateinit var totalText : TextView

    private lateinit var cartDetailsLayout : LinearLayout
    private lateinit var emptyCartImage : ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_cart, container, false)

        subTotalText = rootView.findViewById(R.id.subtotal)
        shippingText = rootView.findViewById(R.id.shipping)
        taxText = rootView.findViewById(R.id.tax)
        totalText = rootView.findViewById(R.id.total)


        cartDetailsLayout = rootView.findViewById(R.id.cartDetailsLayout)
        emptyCartImage = rootView.findViewById(R.id.emptyCartImage)






        fetch()

        var checkOut = rootView.findViewById<Button>(R.id.checkout_btn)
        checkOut.setOnClickListener {
            activity!!. supportFragmentManager.
                beginTransaction().replace(R.id.home_frame,CheckoutFragment()).addToBackStack(null).commit()
        }

        return rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()


        subTotal =0.0
        shipping = 0.0
        tax  = 0.0



        if(call != null && call.isExecuted()) {
            call.cancel();
        }

    }

    fun fetch(){
        Log.d("FETCH====","fetch")
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        call = service.getCartItems(id!!.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<CartResponse> {

            override fun onResponse(call: Call<CartResponse>, response: Response<CartResponse>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE

                Log.e("TAG", " " + Gson().toJson(response.body()))


                    if(!responseJson!!.error){
                        if(responseJson!!.result != null){
                            cartList = ArrayList(responseJson!!.result)
                            for(i in 0..cartList.size-1){
                                subTotal = subTotal + (cartList.get(i).total_amount.toDouble())
                            }

                            Log.d("TOTALAMOUNT---",cartList.get(0).total_amount.toString())
                            Log.d("QUNATITY---",cartList.get(0).qty.toString())
                            Log.d("SUBTOTAL---",subTotal.toString())
                            Log.d("TOTAL---",(subTotal+shipping+tax).toString())

                            subTotalText.setText("$ "+subTotal.toString())
                            shippingText.setText("$ "+shipping.toString())
                            taxText.setText("$ "+tax.toString())
                            totalText.setText("$ "+ (subTotal+shipping+tax).toString())


                            //size adapter
                            val cartRecycler = rootView.findViewById<RecyclerView>(R.id.cart_recycler)
                            cartRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
                            adapter = CartAdapter(activity!!,cartList,this@CartFragment)
                            cartRecycler.adapter = adapter
                        }




                    }
                    else {
                        cartDetailsLayout.visibility = View.GONE
                        emptyCartImage.visibility = View.VISIBLE
                        var checkOut = rootView.findViewById<Button>(R.id.checkout_btn)
                        checkOut.visibility= View.GONE
                    }

                Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

            }

            override fun onFailure(call: Call<CartResponse>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                if (call.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }


    override fun onremoveCart(cartId: Int, position: Int) {

        Log.d("CARTID---",cartId.toString())
        Log.d("POSITION---",position.toString())
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.removeCartItem(id!!.toString(),cartId.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<ErrorWithMessage> {


            override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE


                Log.e("TAG", " " + Gson().toJson(response.body()))



                Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()


                Log.d("ANSWER--------",responseJson!!.error.toString())

                if(responseJson!!.error == false ){

                    cartList.removeAt(position);
                    adapter.notifyDataSetChanged()

                    var cart_text= (activity as HomeActivity).findViewById<TextView>(R.id.cart_text)
                    var quat = cart_text.text.toString().toInt()
                    quat = quat - 1
                    cart_text.setText(quat.toString())

                    subTotal =0.0
                    shipping = 0.0
                    tax  = 0.0

                    subTotalText.setText("$ "+subTotal.toString())
                    shippingText.setText("$ "+shipping.toString())
                    taxText.setText("$ "+tax.toString())
                    totalText.setText("$ "+ (subTotal+shipping+tax).toString())


                    fetch()


                }

            }

            override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)
                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE

                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })
    }





}


