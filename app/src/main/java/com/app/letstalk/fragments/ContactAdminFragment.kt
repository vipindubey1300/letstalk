package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.android.material.navigation.NavigationView
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ContactAdminFragment : Fragment() {

    private  lateinit var rootView :View

    private lateinit var subjectText : EditText
    private lateinit var messageText : EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_contact_admin, container, false)

        (activity as HomeActivity).findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.contact_admin);


        messageText = rootView.findViewById(R.id.message)
        subjectText = rootView.findViewById(R.id.subject)


        var submit= rootView.findViewById<Button>(R.id.submit_btn)
        submit.setOnClickListener {
           // (activity as HomeActivity).filter("gettting filtes")

            onContact(subjectText.text.toString(),messageText.text.toString())

        }




//        var cart_relative = (activity as HomeActivity).findViewById<RelativeLayout>(R.id.cart_relative)
//
//        var notification_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.notification_btn)
//        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)
//
//
//        //convert dp to pixels coz Linear layout params accept height /weight
//        // in pixel
//
//        var height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, getResources().getDisplayMetrics());
//
//        cart_relative.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.5f)
//        notification_btn.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.5f)
//        filter_btn.visibility =View.VISIBLE


        return rootView
    }


    fun validateForm(subject : String,message :String) : Boolean{
        var valid = false

        if(subject.trim().length > 0 && message.trim().length > 0
           ){
            return true
        }

        else if(subject.trim().length == 0){
            // Toast.makeText(activity!!,"Enter Email",Toast.LENGTH_SHORT).show()
            subjectText.requestFocus()
            subjectText.error ="Enter Subject"
            return false
        }

        else if(message.trim().length == 0){
            // Toast.makeText(activity!!,"Enter Password",Toast.LENGTH_SHORT).show()
            messageText.requestFocus()
            messageText.error ="Enter Message"
            return false
        }
        return valid

    }

    fun onContact(subject : String,message :String ){

        if(validateForm(subject,message)){
            Log.d("RESULT---->>","true")

//            val mainIntent = Intent(activity!! as LoginActivity, HomeActivity::class.java)
//            (activity!! as LoginActivity).startActivity(mainIntent)
//            (activity!! as LoginActivity).finish()


            var auth_token : String? = null
            var id  : Int? = null

            //api call

            val PREFS_FILENAME = "user_details"
            val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

            if(prefs.contains("id")) {
                //means logged in

                auth_token = prefs.getString("auth_token","")
                id = prefs.getInt("id",0)


            }


            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.onContact(subject,message,id!!.toString(),auth_token!!.toString())

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE



            call.enqueue(object : Callback<ErrorWithMessage> {
                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {
                    //progressDoalog.dismiss()
//                Toast.makeText(this@MainActivity, response.body()!![0].title, Toast.LENGTH_SHORT).show()
                    var responseJson = response.body()
                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()


                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    if(responseJson!!.error == false ) {

                        subjectText.text.clear()
                        messageText.text.clear()

                        activity!!.supportFragmentManager.beginTransaction().
                            replace(R.id.home_frame,HomeFragment()).addToBackStack(null).commit()

                    }


                    //List<RetroPhoto> photoList
                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE



                    // progressDoalog.dismiss()
                    //Toast.makeText(this@MainActivity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                    Log.d("SADFSDFASDFSDFSDFSD",t.message)

                    // progressDoalog.dismiss()

                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }
        else{
            Log.d("RESULT---->>","false")
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
//
//        var cart_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.cart_btn)
//        var cart_relative = (activity as HomeActivity).findViewById<RelativeLayout>(R.id.cart_relative)
//
//        var notification_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.notification_btn)
//        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)
//
//
//
//        var height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, getResources().getDisplayMetrics());
//
//        cart_relative.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.8f)
//        notification_btn.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.8f)
//        filter_btn.visibility =View.GONE

    }
}
