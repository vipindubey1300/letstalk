package com.app.letstalk.fragments


import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPasswordFragment : Fragment() {

    private lateinit var rootView :View
    private lateinit var emailEdit : EditText
    private lateinit var newPassEdit : EditText
    private lateinit var cnfpassEdit : EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_reset_password, container, false)

        emailEdit = rootView.findViewById(R.id.email_address)
        newPassEdit = rootView.findViewById(R.id.new_password)
        cnfpassEdit = rootView.findViewById(R.id.confirm_password)

        //(activity!! as HomeActivity).supportActionBar?.hide()

        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var h = height * 0.3

        // Gets linearlayout
        val layout: RelativeLayout = rootView.findViewById<RelativeLayout>(R.id.top_layout)
        // Gets the layout params that will allow you to resize the layout
        val params: ViewGroup.LayoutParams = layout.layoutParams
        // params.width = 100
        params.height = h.toInt()
        layout.layoutParams = params

        val reset_btn = rootView.findViewById<Button>(R.id.reset_submit)
        reset_btn.setOnClickListener {

//            activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,OtpFragment()).addToBackStack(null).commit()
            resetPassword(emailEdit.text.toString(),newPassEdit.text.toString(),cnfpassEdit.text.toString())


        }
        val email : String = arguments!!.getString("email")


        emailEdit.setText(email)




        return  rootView
    }

    fun isValid(email:String):Boolean {
        val regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
        return email.matches((regex).toRegex())
    }



    fun isValid(email: String,newPassword: String,cnfPassword: String) : Boolean{

        var valid = false


        if(email.toString().trim().length > 0
            && newPassword.toString().trim().length > 7
            && cnfPassword.toString().trim().length > 7){
            return true
        }
        else if (email.trim().length == 0){
            emailEdit.requestFocus()
            emailEdit.error ="Enter email"
            return false
        }
        else if(!isValid(email)){
            emailEdit.requestFocus()
            emailEdit.error = "Enter Validate Email"
            return false
        }
        else if (newPassword.trim().length == 0){
            newPassEdit.requestFocus()
            newPassEdit.error ="Enter new Password"
            return false
        }
        else if (newPassword.trim().length < 8){
            newPassEdit.requestFocus()
            newPassEdit.error ="New Password should be 8 character long"
            return false
        }
        else if (cnfPassword.trim().length == 0){
            cnfpassEdit.requestFocus()
            cnfpassEdit.error ="Enter Confirm Password"
            return false
        }
        else if (cnfPassword.trim().length < 8){
            cnfpassEdit.requestFocus()
            cnfpassEdit.error ="Confirm Password should be 8 character long"
            return false
        }
        else if (cnfPassword != newPassword){
            cnfpassEdit.requestFocus()
            cnfpassEdit.error ="Both password should match"
            return false
        }
        return  valid



    }

    fun resetPassword(email:String , newPassword : String ,cnfPassword : String){

        if(isValid(email,newPassword,cnfPassword)){


            val email : String = arguments!!.getString("email")


            Log.d("EMAIL--------",email)


            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.resetPassword(email,newPassword,cnfPassword)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE

            call.enqueue(object : Callback<ErrorWithMessage> {


                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                    var responseJson = response.body()

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){



                        activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,LoginFragment()).addToBackStack(null).commit()

                    }

                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }


    }


//    override fun onDestroyView() {
//        super.onDestroyView()
//        (activity!! as HomeActivity).supportActionBar?.show()
//    }
}
