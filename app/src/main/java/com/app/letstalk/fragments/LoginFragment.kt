package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.letstalk.R

import android.graphics.Paint
import android.util.Log
import android.widget.*
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {


    private lateinit var rootView :View



    private lateinit var email_edit : EditText
    private lateinit var password_edit : EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        rootView =  inflater.inflate(R.layout.fragment_login, container, false)

        email_edit = rootView.findViewById(R.id.email_address)
        password_edit = rootView.findViewById(R.id.password)

        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var h = height * 0.3

        // Gets linearlayout
        val layout: RelativeLayout = rootView.findViewById<RelativeLayout>(R.id.top_layout)
        // Gets the layout params that will allow you to resize the layout
        val params: ViewGroup.LayoutParams = layout.layoutParams
        // params.width = 100
        params.height = h.toInt()
        layout.layoutParams = params

        val signup_btn = rootView.findViewById<Button>(R.id.signup_btn)
        signup_btn.setOnClickListener {
            activity!!. supportFragmentManager.popBackStack()
        }

        val termsText : TextView = rootView.findViewById(R.id.terms_text)
        val privacyText : TextView = rootView.findViewById(R.id.privacy_text)

        termsText.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction()
                .replace(R.id.login_frame,TermsConditionFragment())
                .addToBackStack(null).commit()

        }
        privacyText.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction()
                .replace(R.id.login_frame,PrivacyPolicyFragment())
                .addToBackStack(null).commit()

        }


        val forgot_text = rootView.findViewById<TextView>(R.id.forgot_text)
        forgot_text.setPaintFlags(forgot_text.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        forgot_text.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,ForgotPasswordFragment()).addToBackStack(null).commit()
        }

        val signin_btn = rootView.findViewById<Button>(R.id.signin_btn)
        signin_btn.setOnClickListener {


                onLogin(email_edit.text.toString().trim(),password_edit.text.toString().trim())

        }


        return rootView
    }


    fun isValid(email:String):Boolean {
        val regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
        return email.matches((regex).toRegex())
    }



    fun validateLogin(email : String,password :String) : Boolean{
        var valid = false

        if(email.trim().length > 0 && password.trim().length > 0
            && isValid(email)){
            return true
        }

        else if(email.trim().length == 0){
           // Toast.makeText(activity!!,"Enter Email",Toast.LENGTH_SHORT).show()
            email_edit.requestFocus()
            email_edit.error ="Enter email"
            return false
        }
        else if(!isValid(email)){
            //Toast.makeText(activity!!,"Enter Validate Email",Toast.LENGTH_SHORT).show()
            email_edit.requestFocus()
            email_edit.error ="Enter valid Email"
            return false
        }
        else if(password.trim().length == 0){
           // Toast.makeText(activity!!,"Enter Password",Toast.LENGTH_SHORT).show()
            password_edit.requestFocus()
            password_edit.error ="Enter Password"
            return false
        }
        return valid

    }


    fun onLogin(email : String,password :String ){

        if(validateLogin(email,password)){
            Log.d("RESULT---->>","true")

//            val mainIntent = Intent(activity!! as LoginActivity, HomeActivity::class.java)
//            (activity!! as LoginActivity).startActivity(mainIntent)
//            (activity!! as LoginActivity).finish()




            //api call

            var fcm_token = FirebaseInstanceId.getInstance().token

            Log.d("FCM",fcm_token)


            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.onLogin(email,password,"1",fcm_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE



            call.enqueue(object : Callback<LoginResponse> {
                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    //progressDoalog.dismiss()
//                Toast.makeText(this@MainActivity, response.body()!![0].title, Toast.LENGTH_SHORT).show()
                    var responseJson = response.body()
                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()


                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    if(responseJson!!.error == false ){

                        val PREFS_FILENAME = "user_details"
                        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                        val editor = prefs.edit()
                        //editor.putBoolean("loggedIn",true)
                        //editor.putInt("id",id)
                        editor.putString("name",responseJson!!.result.name)
                        editor.putString("email",responseJson!!.result.email)
                        editor.putString("image",responseJson!!.result.user_image)
                        editor.putString("auth_token",responseJson!!.result.auth_token)
                        editor.putInt("id",responseJson!!.result.id)
                        editor.apply()

                        val mainIntent = Intent(activity!! as LoginActivity, HomeActivity::class.java)
                        (activity!! as LoginActivity).startActivity(mainIntent)
                        (activity!! as LoginActivity).finish()

                    }
                    Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
                    //List<RetroPhoto> photoList
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE



                    // progressDoalog.dismiss()
                    //Toast.makeText(this@MainActivity, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                    Log.d("SADFSDFASDFSDFSDFSD",t.message)

                    // progressDoalog.dismiss()

                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }
        else{
            Log.d("RESULT---->>","false")
        }
    }


}
