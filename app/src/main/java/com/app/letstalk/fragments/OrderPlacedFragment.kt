package com.app.letstalk.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.app.letstalk.R

class OrderPlacedFragment : Fragment() {

    private lateinit var rootView : View
    private lateinit var priceText : TextView
    private lateinit var orderTrackingText : TextView
    private lateinit var continueButton : Button


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_order_placed, container, false)

        priceText = rootView.findViewById(R.id.price_text)
        orderTrackingText = rootView.findViewById(R.id.order_tracking_text)
        continueButton = rootView.findViewById(R.id.continue_shopping_btn)

        val price = arguments!!.getString("total_price")
        priceText.setText("$ "+ price.toString())


        orderTrackingText.setOnClickListener {
            activity!!.supportFragmentManager.popBackStack()
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.home_frame,MyOrdersFragment()).addToBackStack(null).commit()
        }

        continueButton.setOnClickListener {
            activity!!.supportFragmentManager.popBackStack()
            activity!!.supportFragmentManager.beginTransaction().
                replace(R.id.home_frame,HomeFragment()).addToBackStack(null).commit()
        }



        return rootView
    }


}
