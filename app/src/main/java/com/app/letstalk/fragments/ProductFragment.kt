package com.app.letstalk.fragments


import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.CategoriesAdapter
import com.app.letstalk.adapters.ProductAdapter
import com.app.letstalk.adapters.ProductsResponse
import com.app.letstalk.models.CategoryResponse
import com.app.letstalk.models.ProductInfoResponse
import com.app.letstalk.models.SearchProducts
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductFragment : Fragment() , ApplyFilter{
    override fun onFilter() {


        Log.d("LLLLLLL-----","fdsdfdsfdfd")

    }

    private lateinit var rootView : View
    private lateinit var call : Call<ProductsResponse>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_product, container, false)



        val category_name : String = if( arguments!!.getString("category_name") == null ) "" else arguments!!.getString("category_name")
        val category_id : Int =if( arguments!!.getInt("category_id")== null ) 0 else arguments!!.getInt("category_id")



        Log.d("CATEGORY____",category_id.toString())

        val headingText = rootView.findViewById<TextView>(R.id.product_name_text)


        if(category_id == 0 ){
            val obj =  arguments!!.getParcelable<SearchProducts>("myObject")
            makeList(obj)
            headingText.setText("Products")

        }
        else{
            showFilterIcon()
            headingText.setText(category_name)

            fetch(category_id)
        }


        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)
        filter_btn.setOnClickListener {


        if(category_id > 0){

            var frag: FilterFragment = FilterFragment(this@ProductFragment)
            var bundle: Bundle = Bundle()
            bundle.putInt("category_id",category_id )

            Log.d("SSSSSSSSS",category_id.toString())

            frag.arguments = bundle
            activity!!. supportFragmentManager.beginTransaction()
                .replace(R.id.home_frame,frag).addToBackStack(null).commit()
        }
            else{
            Toast.makeText(activity!!, "Filters Cannot be applied to search or filtered products !", Toast.LENGTH_SHORT).show()
        }
        }

        return rootView
    }

    fun makeList(searchProducts : SearchProducts){
        val categoryRecycler = rootView.findViewById<RecyclerView>(R.id.products_recycler)
        if(searchProducts!!.result == null){

        }
        else{
            categoryRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
            categoryRecycler.adapter = ProductAdapter(activity!!, ArrayList(searchProducts!!.result))
        }
    }

    fun showFilterIcon(){
        var cart_relative = (activity as HomeActivity).findViewById<RelativeLayout>(R.id.cart_relative)

        var notification_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.notification_btn)
        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)


        //convert dp to pixels coz Linear layout params accept height /weight
        // in pixel

        var height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, getResources().getDisplayMetrics());

        cart_relative.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.5f)
        notification_btn.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.5f)
        filter_btn.visibility =View.VISIBLE
    }

    fun fetch(categoryId : Int){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        call = service.getProductInCategory(id!!.toString(),categoryId.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<ProductsResponse> {


            override fun onResponse(call: Call<ProductsResponse>, response: Response<ProductsResponse>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE


                Log.e("TAG", " " + Gson().toJson(response.body()))



                if(responseJson!!.error == false ){

                    val categoryRecycler = rootView.findViewById<RecyclerView>(R.id.products_recycler)
                           if(responseJson!!.result == null){

                           }
                            else{
                               categoryRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
                               categoryRecycler.adapter = ProductAdapter(activity!!, ArrayList(responseJson!!.result))
                   }

                }

            }

            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {

                if (call.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }

    override fun onDestroyView() {
        super.onDestroyView()

        var cart_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.cart_btn)
        var cart_relative = (activity as HomeActivity).findViewById<RelativeLayout>(R.id.cart_relative)

        var notification_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.notification_btn)
        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)



        var height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, getResources().getDisplayMetrics());

        cart_relative.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.8f)
        notification_btn.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.8f)
        filter_btn.visibility =View.GONE


        if(::call.isInitialized) {
            if(call != null && call.isExecuted()  ){
                call.cancel();
            }

        }

    }


}
