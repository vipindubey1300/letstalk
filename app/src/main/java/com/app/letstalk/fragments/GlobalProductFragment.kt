package com.app.letstalk.fragments


import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.ProductAdapter
import com.app.letstalk.adapters.ProductsResponse
import com.app.letstalk.models.Products
import com.app.letstalk.models.SearchProducts
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class GlobalProductFragment : Fragment() {



    private lateinit var rootView : View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_global_product, container, false)

       val obj =  arguments!!.getParcelable<SearchProducts>("myObject")


        val categoryRecycler = rootView.findViewById<RecyclerView>(R.id.products_recycler)
        if(obj!!.result == null){

        }
        else{
            categoryRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
            categoryRecycler.adapter = ProductAdapter(activity!!, ArrayList(obj!!.result))
        }


        showFilterIcon()

        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)
        filter_btn.setOnClickListener {

//            var frag: FilterFragment = FilterFragment(this@GlobalProductFragment)
//            var bundle: Bundle = Bundle()
//            bundle.putInt("category_id",category_id )
//
//            frag.arguments = bundle
//            activity!!. supportFragmentManager.beginTransaction()
//                .replace(R.id.home_frame,frag).addToBackStack(null).commit()
        }

        return rootView
    }


    fun showFilterIcon(){
        var cart_relative = (activity as HomeActivity).findViewById<RelativeLayout>(R.id.cart_relative)

        var notification_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.notification_btn)
        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)


        //convert dp to pixels coz Linear layout params accept height /weight
        // in pixel

        var height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, getResources().getDisplayMetrics());

        cart_relative.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.5f)
        notification_btn.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.5f)
        filter_btn.visibility =View.VISIBLE
    }


    override fun onDestroyView() {
        super.onDestroyView()

        var cart_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.cart_btn)
        var cart_relative = (activity as HomeActivity).findViewById<RelativeLayout>(R.id.cart_relative)

        var notification_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.notification_btn)
        var filter_btn = (activity as HomeActivity).findViewById<ImageView>(R.id.filter_btn)



        var height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20f, getResources().getDisplayMetrics());

        cart_relative.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.8f)
        notification_btn.layoutParams = LinearLayout.LayoutParams(0,height.toInt(),0.8f)
        filter_btn.visibility =View.GONE


//        if(::call.isInitialized) {
//            if(call != null && call.isExecuted()  ){
//                call.cancel();
//            }
//
//        }

    }



    companion object {
        private val ARG_PARAM = "myObject"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param myObject as MyObject.
         * @return A new instance of fragment MyFragment.
         */
        fun newInstance(myObject:SearchProducts): GlobalProductFragment {
            val fragment = GlobalProductFragment()
            val args = Bundle()
            args.putParcelable(ARG_PARAM, myObject)
            fragment.arguments = args
            return fragment
        }
    }


}
