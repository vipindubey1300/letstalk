package com.app.letstalk.fragments


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.adapters.BottomSheetCategoriesAdapter
import com.app.letstalk.models.Category
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.DividerItemDecoration








class BottomSheet(mcontext: Context, list: ArrayList<Category>) : BottomSheetDialogFragment() {

    var mcontext: Context
    var list: ArrayList<Category>


    init {
        this.mcontext = mcontext
        this.list = list

    }

    private lateinit var rootView: View



//    override fun onAttach(context: Context?) {
//        super.onAttach(context)
//
//    }

   // override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.fragment_bottom_sheet, container, false)

        val categoryRecycler = rootView.findViewById<RecyclerView>(R.id.bottomsheet_category_recycler)
        categoryRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL,false)
      //  categoryRecycler.addItemDecoration(DividerItemDecoration(categoryRecycler.getContext(), DividerItemDecoration.VERTICAL))
        categoryRecycler.adapter = BottomSheetCategoriesAdapter(activity!!,list,this@BottomSheet)

        val dividerItemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        categoryRecycler.addItemDecoration(dividerItemDecoration)

        val closeSheet = rootView.findViewById<ImageView>(R.id.close_sheet)
        closeSheet.setOnClickListener {
            this@BottomSheet.dismiss()
        }

        return rootView
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }
}
