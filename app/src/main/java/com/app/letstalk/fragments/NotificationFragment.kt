package com.app.letstalk.fragments


import android.app.Notification
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.NotificationAdapter
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.models.NotificationResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.Constants
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotificationFragment : Fragment() {

    private lateinit var rootView : View
    var user_id : Int? = null
    var auth_token : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_notification, container, false)

        (activity as HomeActivity).findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.notification);


        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
            //means logged in
            user_id = prefs.getInt("id",0)
            auth_token = prefs.getString("auth_token","")
            getNotifications(user_id!!,auth_token!!)

        }
        else{
            user_id = 0
        }



        return rootView
    }



   fun getNotifications(userId :Int, token:String){

       //api call

       val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
       val call = service.getNotifications(userId.toString(),token)

       //add prgress bar here
       var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
       var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
       frameBackground.visibility = View.VISIBLE
       pBar.visibility = View.VISIBLE




       call.enqueue(object : Callback<NotificationResponse> {


           override fun onResponse(call: Call<NotificationResponse>, response: Response<NotificationResponse>) {


               var responseJson = response.body()

               frameBackground.visibility = View.GONE
               pBar.visibility = View.GONE
               //  pBar.visibility = View.GONE


               Log.e("TAG", " " + Gson().toJson(response.body()))


               // Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



               if(responseJson!!.error == false ){

                   // Picass
                   val notificationRecycler = rootView.findViewById<RecyclerView>(R.id.notification_recycler)


                   notificationRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
                   notificationRecycler.adapter = NotificationAdapter(activity!!, ArrayList(responseJson!!.result.notifications))



                    //this is the valthis sic can be the grat od tht egivan calue of the givan data int he

               }
               else if(responseJson!!.result.notifications.size == 0){

                   Toast.makeText(activity!!, "No notifications found", Toast.LENGTH_SHORT).show()
               }
//               Log.d("RESPONSE----->>>",responseJson!!.message)
//               Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
               //List<RetroPhoto> photoList
           }

           override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {

               Log.d("FAILUREEEEE",t.message)

               frameBackground.visibility = View.GONE
               pBar.visibility = View.GONE

               // progressDoalog.dismiss()
               //pBar.visibility = View.GONE
               Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
           }
       })

    }


}
