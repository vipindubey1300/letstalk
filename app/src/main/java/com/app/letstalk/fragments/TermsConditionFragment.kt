package com.app.letstalk.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.letstalk.R
import com.google.firebase.iid.InstanceIdResult
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId

import android.util.Log
import android.webkit.WebView
import com.app.letstalk.utils.Constants
import com.google.firebase.FirebaseApp


class TermsConditionFragment : Fragment() {

    private lateinit var rootView : View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_terms_condition, container, false)

//        FirebaseApp.initializeApp(activity!!);
//
//        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(activity!!,
//            OnSuccessListener<InstanceIdResult> { instanceIdResult ->
//                val newToken = instanceIdResult.token
//                Log.d("newToken", newToken)
//            })

        val termsWebview : WebView = rootView.findViewById(R.id.terms_web_view)
        termsWebview.loadUrl(Constants.link + "api/api_terms_and_agreement")

        return rootView
    }


}
