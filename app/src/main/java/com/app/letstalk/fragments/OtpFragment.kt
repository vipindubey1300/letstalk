package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.models.SignUpResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class OtpFragment : Fragment() {

    private lateinit var rootView :View

    private lateinit var otpOne : EditText
    private lateinit var otpTwo : EditText
    private lateinit var otpThree : EditText
    private lateinit var otpFour : EditText
    private lateinit var otpFive : EditText
    private lateinit var otpSix : EditText



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_otp, container, false)
        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var h = height * 0.3

        // Gets linearlayout
        val layout: RelativeLayout = rootView.findViewById<RelativeLayout>(R.id.top_layout)
        // Gets the layout params that will allow you to resize the layout
        val params: ViewGroup.LayoutParams = layout.layoutParams
        // params.width = 100
        params.height = h.toInt()
        layout.layoutParams = params


        val email : String? = arguments!!.getString("email")

        val resendOtp = rootView.findViewById<TextView>(R.id.resend_text)
        resendOtp.setOnClickListener {
            onForgot(email!!)
        }

        Log.d("EMAIL___________",email)


        otpOne = rootView.findViewById(R.id.otp_1)
        otpTwo = rootView.findViewById(R.id.otp_2)
        otpThree = rootView.findViewById(R.id.otp_3)
        otpFour = rootView.findViewById(R.id.otp_4)
        otpFive = rootView.findViewById(R.id.otp_5)
        otpSix = rootView.findViewById(R.id.otp_6)

        otpFocus()




        val otpBtn = rootView.findViewById<Button>(R.id.otp_submit)
        otpBtn.setOnClickListener {
            otpVerify(otpOne.text.toString(),
                otpTwo.text.toString(),
                otpThree.text.toString(),
                otpFour.text.toString(),
                otpFive.text.toString(),
                otpSix.text.toString())
        }



        return  rootView
    }

    fun otpFocus(){
        otpOne.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                // TODO Auto-generated method stub
                if (otpOne.getText().length == 1)
                //size as per your requirement
                {
                    otpTwo.requestFocus()
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        otpTwo.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                // TODO Auto-generated method stub
                if (otpTwo.getText().length == 1)
                //size as per your requirement
                {
                    otpThree.requestFocus()
                }

                else  if (otpTwo.getText().length == 0)
                //size as per your requirement
                {
                    otpOne.requestFocus()
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        otpThree.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                // TODO Auto-generated method stub
                if (otpThree.getText().length == 1)
                //size as per your requirement
                {
                    otpFour.requestFocus()
                }

                else  if (otpThree.getText().length == 0)
                //size as per your requirement
                {
                    otpTwo.requestFocus()
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        otpFour.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                // TODO Auto-generated method stub
                if (otpFour.getText().length == 1)
                //size as per your requirement
                {
                    otpFive.requestFocus()
                }
                else  if (otpFour.getText().length == 0)
                //size as per your requirement
                {
                    otpThree.requestFocus()
                }


            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        otpFive.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                // TODO Auto-generated method stub
                if (otpFive.getText().length == 1)
                //size as per your requirement
                {
                    otpSix.requestFocus()
                }
                else  if (otpFive.getText().length == 0)
                //size as per your requirement
                {
                    otpFour.requestFocus()
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        otpSix.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                // TODO Auto-generated method stub

                 if (otpSix.getText().length == 0)
                //size as per your requirement
                {
                    otpFive.requestFocus()
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

    }


    fun isValid(otpOne:String,otpTwo: String,otpThree:String,otpFour: String,otpFive:String,otpSix: String) : Boolean{
        if(otpOne.length == 1 && otpTwo.length==1
            && otpThree.length ==1 && otpFour.length ==1
            && otpFive.length == 1 && otpSix.length == 1){
            return true
        }
        else{
            Toast.makeText(activity!!,"Enter Full OTP",Toast.LENGTH_SHORT).show()
            return false
        }


    }


    fun onForgot(email : String ){


            Log.d("RESULT---->>","true")

            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.forgotPassword(email)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE



            call.enqueue(object : Callback<ErrorWithMessage> {
                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE



                    var responseJson = response.body()

                    Log.d("EERRRORRR", responseJson!!.error.toString())

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                    if(responseJson.error  == false){

                        Log.d("SSSSSSSS",email.toString())

                        var frag: OtpFragment = OtpFragment()
                        var bundle: Bundle = Bundle()
                        bundle.putString("email", email)
                        frag.arguments = bundle

                        activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,frag).commit()
                    }
                    else{
                        otpOne.text.clear()
                        otpTwo.text.clear()
                        otpThree.text.clear()
                        otpFour.text.clear()
                        otpFive.text.clear()
                        otpSix.text.clear()
                    }

                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })


    }

    fun otpVerify(otpone:String,otptwo: String,otpthree:String,otpfour: String,otpfive:String,otpsix: String){

        if(isValid(otpone,otptwo,otpthree,otpfour,otpfive,otpsix)){
            val otp = otpone + otptwo + otpthree + otpfour + otpfive + otpsix

            val email : String = arguments!!.getString("email")

            Log.d("EMAIL--------",email)
            Log.d("OTP--------",otp)


            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.otpVerify(email,otp)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE

            call.enqueue(object : Callback<ErrorWithMessage> {


               // Log.d("RESU:T--------------",response.body().to)


                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                    var responseJson = response.body()
                    Log.d("RESU:T--------------",response.body().toString())

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                   // Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){

                       // val email : String = arguments!!.getString("email")

                        var frag: ResetPasswordFragment = ResetPasswordFragment()
                        var bundle: Bundle = Bundle()
                        bundle.putString("email", email)
                        frag.arguments = bundle

                        activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,frag).addToBackStack(null).commit()

                    }
                    else{
                        otpOne.text.clear()
                        otpTwo.text.clear()
                        otpThree.text.clear()
                        otpFour.text.clear()
                        otpFive.text.clear()
                        otpSix.text.clear()
                    }

                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                    Log.d("ERRORRR----------",t.message)

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }


        }



}



//var product_desc_frag: ProductDescriptionFragment = ProductDescriptionFragment()
//var bundle: Bundle = Bundle()
//bundle.putInt("product_id", product.product_id)
//bundle.putInt("category_id", product.category_id)
//product_desc_frag.arguments = bundle
//
//var context = mcontext as AppCompatActivity
////context.supportFragmentManager.beginTransaction().replace(R.id.recycler_products,product_frag).commit()
////context.supportFragmentManager.beginTransaction().replace(R.id.frame_lay,product_frag).commit()
//
//context.supportFragmentManager.beginTransaction().setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out).replace(R.id.frame_lay, product_desc_frag).addToBackStack(null).commit()
