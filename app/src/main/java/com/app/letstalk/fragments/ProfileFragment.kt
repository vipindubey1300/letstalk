package com.app.letstalk.fragments


import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.opengl.ETC1.encodeImage
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.models.SignUpResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.Constants
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfileFragment : Fragment() {

    private lateinit var rootView : View
    var user_id : Int? = null
    var auth_token : String? = null

    private lateinit var userImage :ImageView
    private lateinit var userName:TextView
    private lateinit var userEmail:TextView
    private lateinit var userPhone:TextView

    private lateinit var firstName : String
    private lateinit var lastName : String
    private lateinit var emailUser : String
    private lateinit var phoneUser : String
    private lateinit var imageUser : String



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_profile, container, false)

        (activity as HomeActivity).findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.account);


        val resetLayout = rootView.findViewById<LinearLayout>(R.id.reset_password)
        val addressLayout = rootView.findViewById<LinearLayout>(R.id.change_address)

        userImage = rootView.findViewById(R.id.profile_image)
        userName = rootView.findViewById(R.id.name)
        userEmail = rootView.findViewById(R.id.email)
        userPhone = rootView.findViewById(R.id.phone)

        resetLayout.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame,ChangePasswordFragment()).addToBackStack(null).commit()
        }

        addressLayout.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame,AddressFragment()).addToBackStack(null).commit()
        }

        var editText = rootView.findViewById<TextView>(R.id.edit_profile)
        editText.setPaintFlags(editText.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)



        editText.setOnClickListener {

            if(::firstName.isInitialized && ::lastName.isInitialized &&
                ::emailUser.isInitialized && ::phoneUser.isInitialized && ::imageUser.isInitialized){


                var frag: EditProfileFragment = EditProfileFragment()
                var bundle: Bundle = Bundle()
                bundle.putString("first_name", if(firstName == null && !::firstName.isInitialized)  "" else firstName)
                bundle.putString("last_name", if(lastName == null && !::lastName.isInitialized)  "" else lastName)
                bundle.putString("email", if(emailUser == null && !::emailUser.isInitialized)  "" else emailUser)
                bundle.putString("mobile", phoneUser)
                bundle.putString("user_image", imageUser)
                frag.arguments = bundle


                activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame,frag).addToBackStack(null).commit()

            }
            else{

                var frag: EditProfileFragment = EditProfileFragment()
                var bundle: Bundle = Bundle()
                bundle.putString("first_name", "")
                bundle.putString("last_name", "")
                bundle.putString("email", "")
                bundle.putString("mobile", "")
                bundle.putString("user_image", "")
                frag.arguments = bundle


                activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame,frag).addToBackStack(null).commit()
            }



        }

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
            //means logged in
            user_id = prefs.getInt("id",0)
            auth_token = prefs.getString("auth_token","")
            getProfile(user_id!!,auth_token!!)

        }
        else{
            user_id = 0
        }


        return rootView
    }

    fun getProfile(userId :Int, token:String ){


            //Log.d("RESULT---->>","true")





            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.getProfile(userId.toString(),token)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE




            call.enqueue(object : Callback<LoginResponse> {


                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {


                    var responseJson = response.body()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                   // Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){

                       // Picasso.with(context).load(Constants.link+responseJson.result.user_image).into(userImage)

                        Picasso.with(context).load(Constants.link+responseJson.result.user_image)
                            .resize(150,150)
                            .into(userImage, object: com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    val imageBitmap = (userImage.getDrawable() as BitmapDrawable).getBitmap()
                                    val imageDrawable = RoundedBitmapDrawableFactory.create(context!!.getResources(), imageBitmap)
                                    imageDrawable.setCircular(true)
                                    imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f)
                                    userImage.setImageDrawable(imageDrawable)
                                }
                                override fun onError() {
                                    //viewHolder.color_image.setImageResource(R.drawable.default_image)
                                }
                            })


                        userName.text = responseJson.result.name
                        userEmail.text = responseJson.result.email
                        userPhone.text = responseJson.result.mobile


                        firstName = responseJson.result.first_name
                        lastName = responseJson.result.last_name
                        emailUser = responseJson.result.email
                        phoneUser = responseJson.result.mobile
                        imageUser = responseJson.result.user_image


                    }
                    else{
                        userName.text = "Not found"
                        userEmail.text = "Not found"
                    }
                    Log.d("RESPONSE----->>>",responseJson!!.message)
                    Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
                    //List<RetroPhoto> photoList
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {

                    Log.d("FAILUREEEEE",t.message)

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    // progressDoalog.dismiss()
                    //pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })

    }



}
