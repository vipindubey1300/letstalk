package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.CategoriesAdapter
import com.app.letstalk.models.CategoryResponse
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CategoriesFragment : Fragment() {

    private lateinit var rootView : View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_categories, container , false)

        val category_name: String = arguments!!.getString("category_name")
        val category_id : Int = arguments!!.getInt("category_id")

        val headingText = rootView.findViewById<TextView>(R.id.category_name_text)
        headingText.setText(category_name)
        fetch(category_id)


        return rootView
    }

    fun fetch(categoryId : Int){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.getCategoryData(id!!.toString(),categoryId.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<CategoryResponse> {


            override fun onResponse(call: Call<CategoryResponse>, response: Response<CategoryResponse>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE


                Log.e("TAG", " " + Gson().toJson(response.body()))



                if(responseJson!!.error == false ){


                    val categoryRecycler = rootView.findViewById<RecyclerView>(R.id.categories_recycler)
                    categoryRecycler.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL ,false)
                    categoryRecycler.adapter = CategoriesAdapter(activity!!, ArrayList(responseJson!!.result))

                }

            }

            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })

    }


}
