package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChangePasswordFragment : Fragment() {

    private lateinit var rootView : View

    private lateinit var oldPassEdit : EditText
    private lateinit var newPassEdit : EditText
    private lateinit var cnfpassEdit : EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_change_password, container, false)

        (activity!! as HomeActivity).supportActionBar?.hide()

        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var h = height * 0.3

        val layout: RelativeLayout = rootView.findViewById<RelativeLayout>(R.id.top_layout)
        // Gets the layout params that will allow you to resize the layout
        val params: ViewGroup.LayoutParams = layout.layoutParams
        // params.width = 100
        params.height = h.toInt()
        layout.layoutParams = params


        oldPassEdit = rootView.findViewById(R.id.old_password)
        newPassEdit = rootView.findViewById(R.id.new_password)
        cnfpassEdit = rootView.findViewById(R.id.confirm_password)


        val change_btn = rootView.findViewById<Button>(R.id.change_submit)
        change_btn.setOnClickListener {
            changePassword(oldPassEdit.text.toString(),newPassEdit.text.toString(),cnfpassEdit.text.toString())
        }



        return rootView
    }


    fun isValid(oldPassword: String,newPassword: String,cnfPassword: String) : Boolean{

        var valid = false


        if(oldPassword.toString().trim().length > 0
            && newPassword.toString().trim().length > 7
            && cnfPassword.toString().trim().length > 7){
            return true
        }
        else if (oldPassword.trim().length == 0){
           oldPassEdit.requestFocus()
            oldPassEdit.error ="Enter old password"
            return false
        }

        else if (newPassword.trim().length == 0){
            newPassEdit.requestFocus()
            newPassEdit.error ="Enter new Password"
            return false
        }
        else if (newPassword.trim().length < 8){
            newPassEdit.requestFocus()
            newPassEdit.error ="New Password should be 8 character long"
            return false
        }
        else if (cnfPassword.trim().length == 0){
            cnfpassEdit.requestFocus()
            cnfpassEdit.error ="Enter Confirm Password"
            return false
        }
        else if (cnfPassword.trim().length < 8){
            cnfpassEdit.requestFocus()
            cnfpassEdit.error ="Confirm Password should be 8 character long"
            return false
        }
        else if (cnfPassword != newPassword){
            cnfpassEdit.requestFocus()
            cnfpassEdit.error ="Both password should match"
            return false
        }
        return  valid



    }

    fun changePassword(oldPassword: String, newPassword : String ,cnfPassword : String){

        if(isValid(oldPassword,newPassword,cnfPassword)){

            var auth_token : String? = null
            var id  : Int? = null

            //api call

            val PREFS_FILENAME = "user_details"
            val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

            if(prefs.contains("id")) {
                //means logged in

                auth_token = prefs.getString("auth_token","")
                id = prefs.getInt("id",0)


            }

            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.updatePassword(id!!.toString(),oldPassword,newPassword,cnfPassword,auth_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE

            call.enqueue(object : Callback<ErrorWithMessage> {


                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                    var responseJson = response.body()

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    //  pBar.visibility = View.GONE

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){


//
//                        val mainIntent = Intent(activity!! as HomeActivity, HomeActivity::class.java)
//                        (activity!! as HomeActivity).startActivity(mainIntent)
//                        (activity!! as HomeActivity).finish()

                        val PREFS_FILENAME = "user_details"
                        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                        val editor = prefs.edit()
                        editor.remove("name")
                        editor.remove("email")
                        editor.remove("image")
                        editor.remove("auth_token")
                        editor.remove("id")
                        editor.commit()


                        val mainIntent = Intent(activity!!, LoginActivity::class.java)
                        activity!!.startActivity(mainIntent)
                        activity!!.finish()

                    }

                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }


    }


        override fun onDestroyView() {
        super.onDestroyView()
        (activity!! as HomeActivity).supportActionBar?.show()
    }



}
