package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.util.DisplayMetrics
import android.util.Log
import android.widget.*
import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.models.SignUpResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignupFragment : Fragment() {

    private lateinit var rootView :View

    private lateinit var fname_edit : EditText
    private lateinit var lname_edit : EditText
    private lateinit var email_edit : EditText
    private lateinit var password_edit : EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        rootView = inflater.inflate(R.layout.fragment_signup, container, false)


        fname_edit = rootView.findViewById(R.id.first_name)
        lname_edit = rootView.findViewById(R.id.last_name)
        email_edit = rootView.findViewById(R.id.email_address)
        password_edit = rootView.findViewById(R.id.password)


        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var h = height * 0.3

        // Gets linearlayout
        val layout: RelativeLayout = rootView.findViewById<RelativeLayout>(R.id.top_layout)
        // Gets the layout params that will allow you to resize the layout
        val params: ViewGroup.LayoutParams = layout.layoutParams
       // params.width = 100
        params.height = h.toInt()
        layout.layoutParams = params
        // Inflate the layout for this fragment


        val termsText : TextView = rootView.findViewById(R.id.terms_text)
        val privacyText : TextView = rootView.findViewById(R.id.privacy_text)

        termsText.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction()
                .replace(R.id.login_frame,TermsConditionFragment())
                .addToBackStack(null).commit()

        }
        privacyText.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction()
                .replace(R.id.login_frame,PrivacyPolicyFragment())
                .addToBackStack(null).commit()

        }


        val signin_btn = rootView.findViewById<Button>(R.id.signin_btn)
        signin_btn.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame, LoginFragment()).addToBackStack(null).commit()
        }

        val signup_btn = rootView.findViewById<Button>(R.id.signup_btn)
        signup_btn.setOnClickListener {
            onSignUp(fname_edit.text.toString().trim(),lname_edit.text.toString().trim(),email_edit.text.toString().trim(),password_edit.text.toString().trim())
        }




        return rootView
    }
    fun isValid(email:String):Boolean {
        val regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
        return email.matches((regex).toRegex())
    }



    fun validateSignUp(fname:String,lname:String,email : String,password :String) : Boolean{
        var valid = false

        val length = password.trim().length

        if(fname.trim().length > 0 && lname.trim().length > 0 && email.trim().length > 0 && password.trim().length > 7
            && isValid(email)){
            return true
        }
        else if(fname.trim().length == 0){
            //Toast.makeText(activity!!,"Enter First Name", Toast.LENGTH_SHORT).show()
            fname_edit.requestFocus()
            fname_edit.error = "Enter First Name"
            return false
        }
        else if(lname.trim().length == 0){
            //Toast.makeText(activity!!,"Enter Last Name", Toast.LENGTH_SHORT).show()
            lname_edit.requestFocus()
            lname_edit.error = "Enter Last Name"
            return false
        }

        else if(email.trim().length == 0){
            //Toast.makeText(activity!!,"Enter Email", Toast.LENGTH_SHORT).show()
            email_edit.requestFocus()
            email_edit.error = "Enter Email"
            return false
        }
        else if(!isValid(email)){
            //Toast.makeText(activity!!,"Enter Validate Email", Toast.LENGTH_SHORT).show()
            email_edit.requestFocus()
            email_edit.error = "Enter Valid Email"
            return false
        }
        else if(password.trim().length == 0){
            //Toast.makeText(activity!!,"Enter Password", Toast.LENGTH_SHORT).show()
            password_edit.requestFocus()
            password_edit.error = "Enter Password"

            return false
        }
        else if(length < 8){
           // Toast.makeText(activity!!,"Password should be 8 characters long", Toast.LENGTH_SHORT).show()

            password_edit.requestFocus()
            password_edit.error = "Password should be 8 characters long"

            return false
        }
        return valid

    }



    fun onSignUp(fname :String, lname:String ,email : String,password :String ){

        Log.d("RESULT---->>>>>>",(password.trim().length < 8).toString())

        if(validateSignUp(fname,lname,email,password)){

            //api call

            var fcm_token = FirebaseInstanceId.getInstance().token

            Log.d("FCM",fcm_token)


            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.onSignup(fname,lname,email,password,"1",fcm_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE



            Log.d("FNAME----->>>",fname)
            Log.d("LNMAE----->>>",lname)
            Log.d("EMAIL----->>>",email)
            Log.d("FCM----->>>",fcm_token!!)

            call.enqueue(object : Callback<SignUpResponse> {


                override fun onResponse(call: Call<SignUpResponse>, response: Response<SignUpResponse>) {


                    var responseJson = response.body()

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                  //  pBar.visibility = View.GONE

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()



                    if(responseJson!!.error == false ){
                        Log.d("TRUE----->>>",fname)

                        val PREFS_FILENAME = "user_details"
                        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                        val editor = prefs.edit()
                        //editor.putBoolean("loggedIn",true)
                        //editor.putInt("id",id)
                        editor.putString("name",responseJson!!.result.name)
                        editor.putString("email",responseJson!!.result.email)
                        editor.putString("image",responseJson!!.result.user_image)
                        editor.putString("auth_token",responseJson!!.result.auth_token)
                        editor.putInt("id",responseJson!!.result.id)
                        editor.apply()

                        val mainIntent = Intent(activity!! as LoginActivity, HomeActivity::class.java)
                        (activity!! as LoginActivity).startActivity(mainIntent)
                        (activity!! as LoginActivity).finish()

                    }
                    Log.d("RESPONSE----->>>",responseJson!!.message)
                    Log.d("SADFSDFASDFSDFSDFSD",response.body()!!.message)
                    //List<RetroPhoto> photoList
                }

                override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {

                    Log.d("FAILUREEEEE",t.message)

                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE

                    // progressDoalog.dismiss()
                    //pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }
        else{
            Log.d("RESULT---->>","false")
        }
    }



}
