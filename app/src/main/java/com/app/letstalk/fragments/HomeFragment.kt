package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.adapters.BestSellerViewpagerAdapter
import com.app.letstalk.adapters.HomeCategoriesAdapter
import com.app.letstalk.adapters.ProductAdapter
import com.app.letstalk.adapters.ViewpagerAdapter
import com.app.letstalk.models.*
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.MyFirebaseMessagingService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.android.material.navigation.NavigationView
import java.util.*

import com.google.android.material.tabs.TabLayout
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList
import com.google.gson.Gson





class HomeFragment : Fragment() {




    private lateinit var rootView : View
    private  lateinit var viewAllCategories : TextView

    private  var categoryList : ArrayList<Category> = ArrayList()
    private  var bannerList : ArrayList<Banners> = ArrayList()
    private lateinit var call : Call<HomeResponse>

    private lateinit var newProductsText : TextView

    private lateinit var newProducts : List<Products>

    private var cart_count = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_home, container, false)

        var fcm_token = FirebaseInstanceId.getInstance().token


          Log.d("TOEKENNNNN",if(fcm_token == null) "null" else fcm_token)

        newProductsText = rootView.findViewById(R.id.newproducts_text)

        newProductsText.setOnClickListener {

            var frag: ProductFragment = ProductFragment()
            var bundle: Bundle = Bundle()
            bundle.putParcelable("myObject", SearchProducts(false,newProducts))

            frag.arguments = bundle


            activity!!.supportFragmentManager.
                beginTransaction().
                replace(R.id.home_frame,frag).addToBackStack(null).
                commit()
        }

        (activity as HomeActivity).findViewById<NavigationView>(R.id.nav_view).setCheckedItem(R.id.home);

        fetchHome()

        //to change viewpader page automaticallyu

        return  rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        if(call != null && call.isExecuted()) {
            call.cancel();
        }
    }

    fun fetchHome(){

        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }

        Log.d("ID------",id!!.toString())
        Log.d("TOKENNN------",auth_token!!.toString())

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
             call = service.getHome(id!!.toString(),auth_token!!)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE



            call.enqueue(object : Callback<HomeResponse> {
                override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                    //progressDoalog.dismiss()
//                Toast.makeText(this@MainActivity, response.body()!![0].title, Toast.LENGTH_SHORT).show()
                    var responseJson = response.body()
                  // Toast.makeText(activity!!, responseJson!!.result.banners.get(0).file, Toast.LENGTH_SHORT).show()

                    Log.e("TAG", " " + Gson().toJson(response.body()))

                    if(responseJson!!.error == false ){

                        var cart_text= (activity as HomeActivity).findViewById<TextView>(R.id.cart_text)
                        cart_text.setText(responseJson!!.result.cart_count.toString())

                        bannerList = ArrayList(responseJson!!.result.banners)
                        categoryList = ArrayList(responseJson!!.result.category)
                        var featuredCategoryList = responseJson!!.result.featured_category
                        var featuredProductList = responseJson!!.result.featured_products

                        newProducts = ArrayList(responseJson!!.result.featured_products)

                        //recycler view for categories
                        val categoryRecycler = rootView.findViewById<RecyclerView>(R.id.categories_recycler)
                        categoryRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL ,false)
                        categoryRecycler.adapter = HomeCategoriesAdapter(activity!!,categoryList!!)

                        var bottomNavDrawerFragment = BottomSheet(activity!!,categoryList!!)
                        viewAllCategories = rootView.findViewById(R.id.view_all_categories)
                        viewAllCategories.setOnClickListener {
                            bottomNavDrawerFragment.show(activity!!.supportFragmentManager, bottomNavDrawerFragment.tag)
                        }

                        var viewPager = rootView.findViewById<ViewPager>(R.id.viewPager)
                        val tabLayout = rootView.findViewById(R.id.tabDots) as TabLayout
                        viewPager.adapter = ViewpagerAdapter(activity!!,bannerList!!)
                        tabLayout.setupWithViewPager(viewPager)

                        var NUM_PAGES = bannerList.size + 1
                        var currentPage = 0
                        var handler = Handler()
                        var update:Runnable = object:Runnable {
                            public override fun run() {
                                if (currentPage === NUM_PAGES - 1)
                                {
                                    currentPage = 0
                                }
                                viewPager.setCurrentItem(currentPage++, true)
                            }
                        }
                        Timer().schedule(object:TimerTask() {
                            override fun run() {
                                handler.post(update)
                            }
                        }, 100, 1000)


                        //fearured prodcys

                        //fearured prodcys

                        var viewPagerFeatured = rootView.findViewById<ViewPager>(R.id.bestseller_viewPager)
                        val tabLayoutFeatured = rootView.findViewById(R.id.bestseller_tabDots) as TabLayout
                        viewPagerFeatured.adapter = BestSellerViewpagerAdapter(activity!!,ArrayList(featuredCategoryList))
                        tabLayoutFeatured.setupWithViewPager(viewPagerFeatured)

                        var NUM_PAGESFeatured = featuredCategoryList.size + 1
                        var currentPageFeatured = 0
                        var handlerFeatured = Handler()
                        var updateFeatured :Runnable = object:Runnable {
                            public override fun run() {
                                if (currentPageFeatured === NUM_PAGESFeatured - 1)
                                {
                                    currentPageFeatured = 0
                                }
                                viewPagerFeatured.setCurrentItem(currentPageFeatured++, true)
                            }
                        }
                        Timer().schedule(object:TimerTask() {
                            override fun run() {
                                handlerFeatured.post(updateFeatured)
                            }
                        }, 200, 2000)


                        //recycler view for spring bestseller
                        val springRecycler = rootView.findViewById<RecyclerView>(R.id.springbestseller_recycler)
                        springRecycler.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL ,false)
                        springRecycler.adapter = ProductAdapter(activity!!,ArrayList(featuredProductList))



                        frameBackground.visibility = View.GONE
                        pBar.visibility = View.GONE

                    }

                    else{
                        Toast.makeText(activity!!, "Authentication failed !", Toast.LENGTH_SHORT).show()
                        val PREFS_FILENAME = "user_details"
                        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
                        val editor = prefs.edit()
                        editor.remove("name")
                        editor.remove("email")
                        editor.remove("image")
                        editor.remove("auth_token")
                        editor.remove("id")
                        editor.commit()


                        val mainIntent = Intent(activity!!, LoginActivity::class.java)
                        activity!!.startActivity(mainIntent)
                        activity!!.finish()
                    }

                }

                override fun onFailure(call: Call<HomeResponse>, t: Throwable) {

                    if (call.isCanceled()) {
                        //do nothing
                    }else {
                        //show some thing to user ui

                        Log.d("ERROR---",t.message)
                        frameBackground.visibility = View.GONE
                        pBar.visibility = View.GONE
                        Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                    }
                }
            })
        }

    }




