package com.app.letstalk.fragments


import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast

import com.app.letstalk.R
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.activity.LoginActivity
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordFragment : Fragment() {


    private lateinit var rootView :View

    private lateinit var email_edit : EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_forgot_password, container, false)

        email_edit = rootView.findViewById(R.id.email_address)

        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        var h = height * 0.3

        // Gets linearlayout
        val layout: RelativeLayout = rootView.findViewById<RelativeLayout>(R.id.top_layout)
        // Gets the layout params that will allow you to resize the layout
        val params: ViewGroup.LayoutParams = layout.layoutParams
        // params.width = 100
        params.height = h.toInt()
        layout.layoutParams = params

        val forgot_btn = rootView.findViewById<Button>(R.id.forgot_submit)
        forgot_btn.setOnClickListener {
            //activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,ResetPasswordFragment()).addToBackStack(null).commit()
            onForgot(email_edit.text.toString().trim())
        }

        return  rootView
    }



    fun isValid(email:String):Boolean {
        val regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
        return email.matches((regex).toRegex())
    }



    fun validateForgot(email : String) : Boolean{
        var valid = false

        if(email.trim().length > 0
            && isValid(email)){
            return true
        }

        else if(email.trim().length == 0){
            email_edit.requestFocus()
            email_edit.error = "Enter Email"
            return false
        }
        else if(!isValid(email)){
            email_edit.requestFocus()
            email_edit.error = "Enter Valid Email"
            return false
        }

        return valid

    }


    fun onForgot(email : String ){

        if(validateForgot(email)){
            Log.d("RESULT---->>","true")

            //api call

            val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
            val call = service.forgotPassword(email)

            //add prgress bar here
            var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
            var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
            frameBackground.visibility = View.VISIBLE
            pBar.visibility = View.VISIBLE



            call.enqueue(object : Callback<ErrorWithMessage> {
                override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE



                    var responseJson = response.body()

                    Log.d("EERRRORRR", responseJson!!.error.toString())

                    Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                    if(responseJson.error  == false){

                        Log.d("SSSSSSSS",email.toString())

                        var frag: OtpFragment = OtpFragment()
                        var bundle: Bundle = Bundle()
                        bundle.putString("email", email)
                        frag.arguments = bundle

                        activity!!. supportFragmentManager.beginTransaction().replace(R.id.login_frame,frag).addToBackStack(null).commit()
                    }

                }

                override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }
            })
        }

    }


}
