package com.app.letstalk.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.letstalk.R
import com.app.letstalk.adapters.AddressAdapter
import com.app.letstalk.adapters.AddressRemove
import com.app.letstalk.models.Address
import com.app.letstalk.models.AddressResponse
import com.app.letstalk.models.ErrorWithMessage
import com.app.letstalk.models.LoginResponse
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.Constants
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddressFragment : Fragment() , AddressRemove {


    private lateinit var addrlist : ArrayList<Address>
    private lateinit var adapter : AddressAdapter


    override fun onremoveAddress(addr: Address, position: Int) {

        Log.d("REMOVE--------------","done")

        removeAddress(addr.addrId,position)
    }

    private lateinit var rootView : View
    var user_id : Int? = null
    var auth_token : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_address, container, false)

        val addAddress = rootView.findViewById<RelativeLayout>(R.id.add_address_layout)
        addAddress.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame, AddAddressFragment()).addToBackStack(null).commit()
        }

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
            //means logged in
            user_id = prefs.getInt("id",0)
            auth_token = prefs.getString("auth_token","")
            getAddress(user_id!!,auth_token!!)

        }
        else{
            user_id = 0
        }

        return rootView
    }

    fun removeAddress(addrId : String , position: Int){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.deleteAddress(id!!.toString(),addrId,auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<ErrorWithMessage> {


            override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE


                Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()


                Log.d("ANSWER--------",responseJson!!.error.toString())

                if(responseJson!!.error == false ){

                    addrlist.removeAt(position);
                    adapter.notifyDataSetChanged()


                }

            }

            override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

//
                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE

                // progressDoalog.dismiss()
                //pBar.visibility = View.GONE
                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })


    }


    fun getAddress(userId :Int, token:String ){



        //api call

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.getAddress(userId.toString(),token)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE




        call.enqueue(object : Callback<AddressResponse> {


            override fun onResponse(call: Call<AddressResponse>, response: Response<AddressResponse>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE




                if(responseJson!!.error == false ){

                    val recycler = rootView.findViewById(R.id.address_recycler)as RecyclerView
                    recycler.setHasFixedSize(true)
                    //recycler_three.isNestedScrollingEnabled = false
                    recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    addrlist = ArrayList(responseJson.result)
                    adapter = AddressAdapter(activity!!,addrlist,this@AddressFragment)
                    recycler.adapter = adapter




                }
                else{

                }

                //List<RetroPhoto> photoList
            }

            override fun onFailure(call: Call<AddressResponse>, t: Throwable) {


                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE

                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })

    }


}
