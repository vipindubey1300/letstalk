package com.app.letstalk.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.letstalk.R
import com.paypal.android.sdk.payments.PayPalConfiguration
import com.paypal.android.sdk.payments.PayPalService
import android.content.Intent

import com.paypal.android.sdk.payments.PaymentActivity
import com.paypal.android.sdk.payments.PayPalPayment

import java.math.BigDecimal
import com.paypal.android.sdk.i
import android.app.Activity
import com.paypal.android.sdk.e
import org.json.JSONException
import android.R.attr.data
import com.paypal.android.sdk.payments.PaymentConfirmation

import android.util.Log
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.letstalk.activity.HomeActivity
import com.app.letstalk.adapters.AddressAdapter
import com.app.letstalk.adapters.AddressAdapterCheckout
import com.app.letstalk.adapters.AddressRadio
import com.app.letstalk.adapters.CartAdapter
import com.app.letstalk.models.*
import com.app.letstalk.utils.ApiService
import com.app.letstalk.utils.RetrofitInstance
import com.crystal.crystalpreloaders.widgets.CrystalPreloader
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_checkout.*
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CheckoutFragment : Fragment(),AddressRadio {




    private  var PAYPAL_CLIENT_ID  ="ARFiHtTEV2P17X4Hdei6M4vqoLx_UVGAvELcTpNprLC4hNklLmNq7cl_z5OqhRGoBzzVRmS9sG-RfhXx"
    private lateinit var rootView : View


    private lateinit var addrlist : ArrayList<Address>
    private lateinit var adapter : AddressAdapterCheckout

    var user_id : Int? = null
    var auth_token : String? = null

    private var pos = -1
    private var addrId = 0

    private var subTotal =0.0
    private var shipping = 0.0
    private var tax  = 0.0

    private lateinit var subTotalText : TextView
    private lateinit var shippingText : TextView
    private lateinit var taxText : TextView
    private lateinit var totalText : TextView


    private lateinit var call : Call<CartResponse>

    private lateinit var cartList : ArrayList<ResultCart>



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(com.app.letstalk.R.layout.fragment_checkout, container, false)

        subTotalText = rootView.findViewById(R.id.subtotal)
        shippingText = rootView.findViewById(R.id.shipping)
        taxText = rootView.findViewById(R.id.tax)
        totalText = rootView.findViewById(R.id.total)

        val addAddress = rootView.findViewById<RelativeLayout>(R.id.add_address_layout)
        addAddress.setOnClickListener {
            activity!!. supportFragmentManager.beginTransaction().replace(R.id.home_frame, AddAddressFragment()).addToBackStack(null).commit()
        }




        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)
        if(prefs.contains("id")) {
            //means logged in
            user_id = prefs.getInt("id",0)
            auth_token = prefs.getString("auth_token","")
            getAddress(user_id!!,auth_token!!)

        }
        else{
            user_id = 0
        }

        fetch()

        startPaypal()



        val paypal = rootView.findViewById<Button>(R.id.payment_btn)
            paypal.setOnClickListener {
                if(addrId == 0){
                    Toast.makeText(activity!!,"Select address",Toast.LENGTH_SHORT).show()
                }
                else{
                    payPal()
                }
            }





        return rootView
    }

    fun startPaypal(){
        //starting paypal service

        val intent = Intent(activity!!, PayPalService::class.java)
        //Paypal Configuration Object
        val config =  PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PAYPAL_CLIENT_ID)

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)

        activity!!.startService(intent)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        if(call != null && call.isExecuted()) {
            call.cancel();
        }

    }


    fun createOrder(txnId:String){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val total = subTotal+shipping+tax
        val call = service.createOrder(id!!.toString(),auth_token!!,addrId.toString(),txnId,total.toString(),"approved")

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE


        call.enqueue(object : Callback<ErrorWithMessage> {

            override fun onResponse(call: Call<ErrorWithMessage>, response: Response<ErrorWithMessage>) {


                Log.e("TAG", " " + Gson().toJson(response.body()))


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE


                Toast.makeText(activity!!, responseJson!!.message, Toast.LENGTH_SHORT).show()

                     if(responseJson!!.error ==  false){

                         var cart_text= (activity as HomeActivity).findViewById<TextView>(R.id.cart_text)
                         var quat = cart_text.text.toString().toInt()
                         quat = 0
                         cart_text.setText(quat.toString())

                         var frag: OrderPlacedFragment = OrderPlacedFragment()
                         var bundle: Bundle = Bundle()
                         bundle.putString("total_price",(shipping+subTotal+tax).toString())

                         frag.arguments = bundle

                         activity!!. supportFragmentManager.
                            popBackStack()
                         activity!!. supportFragmentManager.
                             beginTransaction().replace(R.id.home_frame,frag).addToBackStack(null).commit()





                     }





            }

            override fun onFailure(call: Call<ErrorWithMessage>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                if (call.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }


    fun fetch(){
        var auth_token : String? = null
        var id  : Int? = null

        //api call

        val PREFS_FILENAME = "user_details"
        val prefs = activity!!.applicationContext.getSharedPreferences(PREFS_FILENAME,0)

        if(prefs.contains("id")) {
            //means logged in

            auth_token = prefs.getString("auth_token","")
            id = prefs.getInt("id",0)


        }



        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        call = service.getCartItems(id!!.toString(),auth_token!!)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE



        call.enqueue(object : Callback<CartResponse> {

            override fun onResponse(call: Call<CartResponse>, response: Response<CartResponse>) {


                var responseJson = response.body()
                Log.e("TAG", " " + Gson().toJson(response.body()))

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE
                //  pBar.visibility = View.GONE




                cartList = if(responseJson!!.result == null) ArrayList() else ArrayList(responseJson!!.result)

                for(i in 0..cartList.size-1){
                    subTotal = subTotal + cartList.get(i).total_amount.toDouble()
                }

                subTotalText.setText("$ "+subTotal.toString())
                shippingText.setText("$ "+shipping.toString())
                taxText.setText("$ "+tax.toString())
                totalText.setText("$ "+ (subTotal+shipping+tax).toString())


            }

            override fun onFailure(call: Call<CartResponse>, t: Throwable) {

                Log.d("FAILUREEEEE",t.message)

                if (call.isCanceled()) {
                    //do nothing
                }else {
                    //show some thing to user ui
                    frameBackground.visibility = View.GONE
                    pBar.visibility = View.GONE
                    Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
                }


            }
        })

    }



    override fun onCheckAddress(addr: Int, position: Int) : Boolean {
        if(pos == -1){
            pos = position
            addrId = addr
            return true
        }
        else if(addrId == addr && pos == position){
            return false

        }
        else{
            pos = position
            addrId = addr
            return  true
        }

    }


    fun getAddress(userId :Int, token:String ){



        //api call

        val service = RetrofitInstance.retrofitInstance.create(ApiService::class.java)
        val call = service.getAddress(userId.toString(),token)

        //add prgress bar here
        var pBar = rootView.findViewById<CrystalPreloader>(R.id.pBar)
        var frameBackground = rootView.findViewById<RelativeLayout>(R.id.progress_frame)
        frameBackground.visibility = View.VISIBLE
        pBar.visibility = View.VISIBLE




        call.enqueue(object : Callback<AddressResponse> {


            override fun onResponse(call: Call<AddressResponse>, response: Response<AddressResponse>) {


                var responseJson = response.body()

                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE




                if(responseJson!!.error == false ){

                    val recycler = rootView.findViewById(R.id.address_recycler)as RecyclerView
                    recycler.setHasFixedSize(true)
                    //recycler_three.isNestedScrollingEnabled = false
                    recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    addrlist = ArrayList(responseJson.result)
                    adapter = AddressAdapterCheckout(activity!!,addrlist,this@CheckoutFragment)
                    recycler.adapter = adapter




                }
                else{

                }


            }

            override fun onFailure(call: Call<AddressResponse>, t: Throwable) {


                frameBackground.visibility = View.GONE
                pBar.visibility = View.GONE

                Toast.makeText(activity!!, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show()
            }
        })

    }



    fun payPal(){
        //Creating a paypalpayment
        val payment = PayPalPayment(
            BigDecimal(shipping+subTotal+tax), "USD", "Payment for items",
            PayPalPayment.PAYMENT_INTENT_SALE
        )

        //Creating Paypal Payment activity intent
        val intent = Intent(activity!!, PaymentActivity::class.java)

        val config =  PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PAYPAL_CLIENT_ID)

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment)

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, 123)
    }

    override fun onDestroy() {
        activity!!.stopService(Intent(activity!!, PayPalService::class.java))
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //If the result is from paypal
        if (requestCode === 123) {

            Log.d("PAYAPL----------------",requestCode.toString())

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode === Activity.RESULT_OK) {
                //Getting the payment confirmation
                val confirm = data?.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION) as PaymentConfirmation

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        val paymentDetails = confirm.toJSONObject().toString(4)
                        Log.i("paymentExample", paymentDetails)

                        val txnid =confirm.toJSONObject().getJSONObject("response").getString("id")
                        Log.d("DONE--------------",txnid)
                        createOrder(txnid)


                        //Starting a new activity for the payment details and also putting the payment details with intent
//                        startActivity(
//                            Intent(this, ConfirmationActivity::class.java)
//                                .putExtra("PaymentDetails", paymentDetails)
//                                .putExtra("PaymentAmount", paymentAmount)
//                        )

                    } catch (e: JSONException) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e)
                    }

                }
            } else if (resultCode === Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.")
            } else if (resultCode === PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.")
            }
        }
    }
}
